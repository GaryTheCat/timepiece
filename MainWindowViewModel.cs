﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using Timepiece_V2.Commands;
using Timepiece_V2.Helpers;
using Timepiece_V2.Managers;
using Timepiece_V2.Notifications;
using Timepiece_V2.Services;
using Timepiece_V2.UIControls;
using Timepiece_V2.UIControls.AddSickLeaveWindow;
using Timepiece_V2.UIControls.AllTickets;
using Timepiece_V2.UIControls.RecordEdit;
using Timepiece_V2.UIControls.Recurring;
using Timepiece_V2.UIControls.ThisSprint;
using Timepiece_V2.UIControls.TimeEntryContextMenu;
using Timepiece_V2.UIControls.ViewConductor;
using Timepiece_V2.Workflows;

namespace Timepiece_V2
{
    public class MainWindowViewModel : ReactiveViewModelBase
    {
        public string WindowDisplayName => $"Timepiece {Configuration.DisplayVersion}";

        public MainWindow View { get; set; }

        public WorkflowManagerViewModel WorkflowManagerViewModel;

        public MainWindowViewModel()
        {
            if (!Directory.Exists(Configuration.RootDirectory))
            {
                Directory.CreateDirectory(Configuration.RootDirectory);
            }

            this.CommandHandler = new CommandHandler();

            this.ToastManager = new ToastManager();

            this.EntryManager = new EntryManager();

            this.ToastNotificationViewModel = new ToastNotificationViewModel();

            this.ToastManager.AddToast(ToastNotificationViewModel, ToastLocation.MainWindow);

            this.SettingsWindowViewModel = new SettingsWindowViewModel(this, ToastManager);

            this.EventManager = new ReocurringEventManager();

            this.RepeatEventsViewModel = new RecurringViewModel();

            this.AddSickLeaveWindowViewModel = new AddSickLeaveWindowViewModel(this);

            this.WindowState = new DataStructures.WindowState();

            this.WindowStateManager = new WindowStateManager(this.WindowState, this.OnPropertyChanged);

            this.TabContentManagerViewModel = new TabContentManagerViewModel(this.WindowStateManager);

            this.LeftHandPane = new ConductorViewModel();

            PopulateRightPaneContent();

            PopulateLeftPaneContent();



            this.Load();

            // Set after the load as I cant work out the correct way to load this using JSON.
            this.SettingsWindowViewModel.Settings.ViewModel = this.SettingsWindowViewModel;
            this.SettingsWindowViewModel.Settings.SprintStartDate = new DateTime(2019, 08, 19);
            this.EntryManager.Settings = this.SettingsWindowViewModel.Settings;

            RegisterWindows();

            this.CommandHandler.TryLoadHistory();
        }

        private void PopulateLeftPaneContent()
        {
            var ticketInputViewModel = new TicketInputViewModel(
                this.ToastManager,
                this.EntryManager,
                this,
                this.TabContentManagerViewModel,
                this.SettingsWindowViewModel,
                this.EventManager,
                this.CommandHandler,
                this.LeftHandPane);

            this.LeftHandPane.AddItem("TicketInput", ticketInputViewModel);
            this.LeftHandPane.SetInitialContent("TicketInput");

            var recordEditView = new RecordEditViewModel(this.LeftHandPane, this.ToastManager);
            this.LeftHandPane.AddItem("EditRecord", recordEditView);

            WindowService.RegisterWindow(ticketInputViewModel);
            WindowService.RegisterWindow(recordEditView);
        }

        private void PopulateRightPaneContent()
        {
            var thisSprintViewModel = new ThisSprintViewModel(EntryManager, LeftHandPane) { IsSelected = true };
            
            var allTicketsViewModel = new AllTicketsViewModel(EntryManager);

            thisSprintViewModel.MainWindowViewModel = this;

            this.TabContentManagerViewModel.AddTab(thisSprintViewModel);
            this.TabContentManagerViewModel.AddTab(this.RepeatEventsViewModel);
            this.TabContentManagerViewModel.AddTab(allTicketsViewModel);

            this.TimeEntryContextMenuViewModel = new TimeEntryContextMenuViewModel(LeftHandPane);

            this.TabContentManagerViewModel.SetSelectedTab(thisSprintViewModel);
            
            WindowService.RegisterWindow(TimeEntryContextMenuViewModel);
        }

        private void RegisterWindows()
        {
            WindowService.RegisterWindow(EntryManager);
            WindowService.RegisterWindow(SettingsWindowViewModel);
            WindowService.RegisterWindow(RepeatEventsViewModel);
            WindowService.RegisterWindow(AddSickLeaveWindowViewModel);
            WindowService.RegisterWindow(SettingsWindowViewModel.Settings);
            WindowService.RegisterWindow(this.CommandHandler);
        }

        public ToastManager ToastManager { get; set; }

        public EntryManager EntryManager { get; set; }
        
        public TabContentManagerViewModel TabContentManagerViewModel { get; set; }

        public ConductorViewModel LeftHandPane { get; set; }

        public ToastNotificationViewModel ToastNotificationViewModel { get; set; }

        public SettingsWindowViewModel SettingsWindowViewModel { get; set; }

        public SettingsWindowView SettingsWindowView { get; set; }
        
        public RecurringViewModel RepeatEventsViewModel { get; set; }

        public Deprecated_RepeatEventsView RepeatEventsView { get; set; }

        public ReocurringEventManager EventManager { get; set; }

        public AddSickLeaveWindowView AddSickLeaveWindowView { get; set; }

        public AddSickLeaveWindowViewModel AddSickLeaveWindowViewModel { get; set; }

        public TimeEntryContextMenuViewModel TimeEntryContextMenuViewModel { get; set; }

        public CommandHandler CommandHandler { get; set; }

        public bool FirstLoad { get; set; }
                
        public bool IsSettingsClosed { get; set; } = true;

        public DataStructures.WindowState WindowState { get; set; }

        public WindowStateManager WindowStateManager { get; set; }

        public void ProcessClearClick()
        {
            if (this.LeftHandPane.GetControlModel<TicketInputViewModel>().IsTimerRunning)
            {
                this.ToastManager.ShowToast(
                "Oops",
                $"You cant remove job records while you're creating a new one!",
                3,
                false);
                return;
            }

            //int jobsRemoved = EntryManager.DeleteSelectedTimeEntries();
            var command = new DeleteEntriesCommand(EntryManager.GetTimeEntries().Where(e => e.IsSelected).ToList());
            CommandHandler.Execute(command);

            this.OnPropertyChanged(nameof(this.TotalTime));

            this.ToastManager.ShowToast(
                "Clear Successful",
                $"Records have been successfully removed",
                3,
                true);

            this.LeftHandPane.GetControlModel<TicketInputViewModel>().Update(
                this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries);
        }

        public void ProcessExportClick()
        {
            var isRunning = this.LeftHandPane.GetControlModel<TicketInputViewModel>().IsTimerRunning;

            if (!this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().Days.Any())
            {
                this.ToastManager.ShowToast(
                    "Oops",
                    $"You cant export job records if you don't have any...",
                    3,
                    false);
                return;
            }

            var entries = this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries;
            var selectedEntries = entries.Where(e => e.IsSelected);

            ExportHelper.ExportGoalsFile(
                selectedEntries.Any() ? selectedEntries.ToList() : entries.ToList(), 
                ToastManager, 
                this.SettingsWindowViewModel.Settings);

        }

        public void MinimiseLeftPane()
        {
            WindowStateManager.MinimiseLeftPane();
        }

        public void MaximiseLeftPane()
        {
            WindowStateManager.MaximiseLeftPane();
        }

        public void Load()
        {
            if (File.Exists(Configuration.SettingsJSON))
            {
                this.SettingsWindowViewModel.Load();
                this.EntryManager.Load();
                this.LeftHandPane.GetControlModel<TicketInputViewModel>().Update(
                    this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries);
            }
            else
            {
                this.SettingsWindowViewModel.Settings.CustomExportScript = CompileHelper.GetUserModifiableCode();
                this.FirstLoad = true;
            }

            this.RepeatEventsViewModel.Load();

            this.EventManager.UpdateEvents(this.RepeatEventsViewModel.Events.ToList());
        }

        public void Save()
        {
            this.SettingsWindowViewModel.Settings.IsMaximised =
                this.View.WindowState == System.Windows.WindowState.Maximized;
            this.SettingsWindowViewModel.Settings.WindowWidth = this.View.Width;
            this.SettingsWindowViewModel.Settings.WindowHeight = this.View.Height;

            SettingsWindowViewModel.Save();

            EntryManager.Save();

            RepeatEventsViewModel.Save();
        }

        public void ProcessSettingsClick()
        {
            SettingsWindowView = new SettingsWindowView();
            SettingsWindowView.DataContext = this.SettingsWindowViewModel;

            this.IsSettingsClosed = false;
            SettingsWindowView.Show();
        }

        public void ProcessRepeatEventsClick()
        {
            RepeatEventsView = new Deprecated_RepeatEventsView();
            RepeatEventsView.DataContext = this.RepeatEventsViewModel;

            RepeatEventsView.Show();
        }

        public void ProcessSickLeaveClick()
        {
            AddSickLeaveWindowView = new AddSickLeaveWindowView();
            AddSickLeaveWindowView.DataContext = this.AddSickLeaveWindowViewModel;

            AddSickLeaveWindowView.Show();
        }


        public string TotalTime => GetTotalTimeString();

        public string GetTotalTimeString()
        {
            var currentTime = new TimeSpan(this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.Sum(e => e.Duration));

            var last = this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.Last();
            var duration = last.End - last.Start;
            var hours = ((int)currentTime.TotalHours).ToString("00");
            var minutes = currentTime.Minutes.ToString("00");
            var seconds = currentTime.Seconds.ToString("00");

            double totalMillis = SettingsWindowViewModel.Settings.HoursInSprint * 60 * 60 * 1000;
            var percentage = (currentTime.TotalMilliseconds / totalMillis) * 100;

            return $"Total Time: {hours}:{minutes} ({string.Format("{0:0.00}", percentage)}% of sprint)";
        }
    }
}
