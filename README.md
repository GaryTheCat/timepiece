## Timepiece ##
**Luke Berry**

![Image of Timepiece](https://bitbucket.org/GaryTheCat/timepiece/raw/9be4eef4202a7f3be9b18585f2c6c21552f815d7/Images/Application.PNG)

Timepiece is a simple timekeeping application with a configurable C# based export, allowing your work records to be exported in any format.

- Add recurring events
- Create and track work spent on any task
- Group work done by day / sprint
- Edit existing records with simple and intuitive interface

It's entirely open, so pull, make changes, do whatever the hell you feel like.