﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Timepiece_V2
{
    /// <summary>
    /// Reactive VM - Provides reactive funtionality to windows and ensures any 
    /// window that inherits from this class will fire notification messages to 
    /// keep UI elements updated.
    /// </summary>
    [DataContract]
    public class ReactiveViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string caller = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }
}
