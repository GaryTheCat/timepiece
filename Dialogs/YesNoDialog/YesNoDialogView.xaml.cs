﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Timepiece_V2.Dialogs.YesNoDialog
{
    /// <summary>
    /// Interaction logic for YesNoDialogView.xaml
    /// </summary>
    public partial class YesNoDialogView : Window
    {
        public YesNoDialogViewModel ViewModel => this.DataContext as YesNoDialogViewModel;

        public YesNoDialogView()
        {
            InitializeComponent();
        }
        
        private void No_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.Continue = false;
            this.Close();
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.Continue = true;
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                try
                {
                    this.DragMove();
                }
                catch { }
            }
        }
    }
}
