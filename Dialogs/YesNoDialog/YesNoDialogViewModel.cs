﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timepiece_V2.Dialogs.YesNoDialog
{
    public class YesNoDialogViewModel
    {
        public YesNoDialogViewModel(string message)
        {
            Message = message;
        }

        public bool Show()
        {
            var view = new YesNoDialogView();
            view.DataContext = this;

            view.ShowDialog();

            return Continue;
        }

        public string Message { get; set; }

        public bool Continue { get; set; }
    }
}
