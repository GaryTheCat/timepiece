﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Timepiece_V2.DataStructures;

namespace Timepiece_V2.Helpers
{
    public static class SprintHelper
    {
        public static List<Range<DateTime>> SprintStartDates = new List<Range<DateTime>>();

        public static void WriteSprint(IGrouping<string, TimeEntry> sprint)
        {
            if (!Directory.Exists(Configuration.BackupRecordsDirectory))
            {
                Directory.CreateDirectory(Configuration.BackupRecordsDirectory);
            }
            var recordsToExport = sprint.Select(v => v).ToList();
            var fileName = Configuration.GetBackupRecordsFileLocation(sprint.Key);

            RecordHelper.UpdateRecords(fileName, recordsToExport);
        }

        public static string GetSprintName(DateTime sprintStartDate, DateTime entryStartTime)
        {
            var inRange = SprintStartDates.FirstOrDefault(r => r.IsInRange(sprintStartDate));

            if(inRange != null)
            {
                return GetFormattedString(inRange);
            }

            DateTime currentDateTime = DateTime.MinValue;

            if (SprintStartDates.Any())
            {
                currentDateTime = SprintStartDates.Last().Upper;
            }
            else
            {
                currentDateTime = sprintStartDate;
            }

            while (!SprintStartDates.Any(s => s.IsInRange(entryStartTime)))
            {
                var nextTime = currentDateTime.AddDays(21);
                SprintStartDates.Add(new Range<DateTime>(currentDateTime, nextTime));
                currentDateTime = nextTime;
            }

            return GetFormattedString(SprintStartDates.Last());
        }

        private static string GetFormattedString(Range<DateTime> range)
        {
            return $"{range.Lower.Date.ToString("yyyy-MM-dd")} to {range.Upper.Date.ToString("yyyy-MM-dd")}";
        }
    }
}
