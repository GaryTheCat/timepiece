﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Managers;
using Timepiece_V2.UIControls.ThisSprint;

namespace Timepiece_V2.Helpers
{
    public static class ExportHelper
    {
        public static void ExportGoalsFile(
            IEnumerable<TimeEntry> timeEntries,
            ToastManager toastManager,
            Settings settings)
        {
            var goalsFile = new List<string>();

            if (settings.UseCustomExport)
            {
                var tempPath = settings.ViewModel.Compile(false);

                if(tempPath == null)
                {
                    return;
                }

                _ = Task.Run(() =>
                 {
                     var myProcess = new Process();
                     myProcess.StartInfo.FileName = tempPath;
                     myProcess.StartInfo.UseShellExecute = false;
                     myProcess.StartInfo.CreateNoWindow = true;
                     myProcess.Start();

                     myProcess.WaitForExit();

                     if (File.Exists(settings.GoalsExportLocation + "\\" + settings.FirstName + "." + settings.LastName))
                     {
                         toastManager.ShowToast(
                             "Export Successful",
                             $"{timeEntries.Count()} records have been successfully saved to: '{settings.GoalsExportLocation}'",
                             3,
                             true);
                     }
                     else
                     {
                         toastManager.ShowToast(
                         "Export Failed",
                         $"There was an error when attempting to save to: '{settings.GoalsExportLocation}'. You can modify your save path in the settings.",
                         5,
                         false);
                     }
                 });
            }
            else
            {
                var first = timeEntries.First().Start;
                var currentDate = new DateTime(first.Year, first.Month, first.Day);
                var entriesByDay = new Dictionary<DateTime, List<TimeEntry>>();
                foreach(var entry in timeEntries.OrderBy(e => e.StartTicks))
                {
                    var entryDate = new DateTime(entry.Start.Year, entry.Start.Month, entry.Start.Day);
                    if (!entriesByDay.ContainsKey(entryDate))
                    {
                        entriesByDay.Add(entryDate, new List<TimeEntry>());
                    }

                    entriesByDay[entryDate].Add(entry);
                }

                foreach(var day in entriesByDay)
                {
                    goalsFile.Add($"{day.Key.DayOfWeek}");
                    var jobs = day.Value.GroupBy(e => e.Job);

                    foreach(var job in jobs)
                    {
                        var nonAggregatedJobs = job.Select(j => j).ToList();

                        var aggregatedJobs = AggregateJobs(nonAggregatedJobs);

                        bool printedJob = false;
                        foreach(var entry in aggregatedJobs)
                        {
                            if (entry.ShowJob)
                            {
                                // Print the job only info
                                goalsFile.Add($"\t+ {entry.Job} {entry.Description} {GetDuration(entry)}");
                            }
                            else if (entry.ShowTicket)
                            {
                                // Print the ticket only info
                                goalsFile.Add($"\t+ {entry.Ticket} {entry.Description} {GetDuration(entry)}");
                            }
                            else
                            {
                                // Write the ticket
                                goalsFile.Add($"\t+ {entry.Ticket} {entry.Description} {GetDuration(entry)}");
                            }
                        }
                    }
                }

                try
                {
                    File.WriteAllLines(settings.GoalsExportLocation + "\\" + settings.FirstName + "." + settings.LastName, goalsFile);
                    toastManager.ShowToast(
                        "Export Successful",
                        $"{timeEntries.Count()} records have been successfully saved to: '{settings.GoalsExportLocation}'",
                        3,
                        true);
                }
                catch
                {
                    toastManager.ShowToast(
                    "Export Failed",
                    $"There was an error when attempting to save to: '{settings.GoalsExportLocation}'. You can modify your save path in the settings.",
                    5,
                    false);
                }
            }
        }

        private static List<TimeEntry> AggregateJobs(List<TimeEntry> jobs)
        {
            var aggregatedJobs = new List<TimeEntry>();
            var orderedJobs = jobs.Select(j => j.Clone()).OrderBy(j => j.Ticket).ThenBy(j => j.Description);

            var ticket = string.Empty;
            var description = string.Empty;

            var jobGroups = new Dictionary<Tuple<string, string, string>, List<TimeEntry>>();

            foreach (var job in orderedJobs)
            {
                var key = new Tuple<string, string, string>(job?.Job?.Trim(), job?.Ticket?.Trim(), job?.Description?.Trim());

                if (!jobGroups.ContainsKey(key))
                {
                    jobGroups.Add(key, new List<TimeEntry>());
                }

                jobGroups[key].Add(job);
            }

            foreach (var job in jobGroups)
            {
                var duration = job.Value.Sum(j => (j.End - j.Start).TotalMilliseconds);
                var start = job.Value.Min(j => j.Start);

                aggregatedJobs.Add(new TimeEntry(
                    job.Key.Item1,
                    job.Key.Item2,
                    job.Key.Item3,
                    start,
                    start.AddMilliseconds(duration)));
            }

            return aggregatedJobs;
        }

        public static string GetDuration(TimeEntry entry)
        {
            var duration = TimeSpan.FromTicks((entry.End - entry.Start).Ticks);
            int hours = duration.Hours % 24;
            int minutes = duration.Minutes % 60;
            int seconds = duration.Seconds % 60;

            if (hours == 0 && minutes != 0)
            {
                return "(" + minutes + "m)";
            }
            else if (hours == 0 && minutes == 0)
            {
                return " (" + seconds + "s)";
            }
            else
            {
                return " (" + hours + "h" + minutes + "m)";
            }
        }
    }
}
