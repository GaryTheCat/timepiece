﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Timepiece_V2.DataStructures;

namespace Timepiece_V2.Helpers
{
    public static class CompileHelper
    {
        public static string GetUserModifiableCode()
        {
            string exportCode;

            // Get the code required for export.
            if (File.Exists(Configuration.ExportsFile))
            {
                exportCode = File.ReadAllText(Configuration.ExportsFile);
            }
            else
            {
                exportCode = Properties.Resources.BaseExport;
            }

            // Split the export code into lines
            var lines = exportCode.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();

            // We want to only display a portion of the export code that is user modifiable,
            // these flags tell us where to start and stop.
            int startIndex = lines.IndexOf(lines.First(l => l.Contains("//// HEAD ////"))) + 1;
            int endIndex = lines.IndexOf(lines.First(l => l.Contains("//// TAIL ////")));

            // Create the string that represents the user modifiable code
            var stringBuilder = new StringBuilder();
            for (int i = startIndex; i < endIndex; i++)
            {
                stringBuilder.AppendLine(lines[i]);
            }

            return stringBuilder.ToString();
        }

        public static string UpdateWithUserCode(string populatedCode, string userCode)
        {            
            // Split the export code into lines
            var lines = populatedCode.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();

            // We want to only display a portion of the export code that is user modifiable,
            // these flags tell us where to start and stop.
            int startIndex = lines.IndexOf(lines.First(l => l.Contains("//// HEAD ////")));
            int endIndex = lines.IndexOf(lines.First(l => l.Contains("//// TAIL ////")));

            var userLines = userCode.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();

            // Remove the existing code.
            for (int i = endIndex - 1; i > startIndex; i--)
            {
                lines.RemoveAt(i);
            }

            // Add the new code.
            for(int i = 0; i < userLines.Count(); i++)
            {
                lines.Insert(startIndex + i + 1, userLines[i]);
            }

            // Build the export code again in its updated form.
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < lines.Count; i++)
            {
                stringBuilder.AppendLine(lines[i]);
            }

            return stringBuilder.ToString();
        }

        public static string InsertJobData(List<TimeEntry> jobEntries, Settings settings, string exportCode)
        {
            var lines = exportCode.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();

            // Use the Insert Start flag to identify where job record data should be inserted.
            int startIndex = lines.IndexOf(lines.First(l => l.Contains("//// INSERT START ////"))) + 1;

            // Update all the user information - Export Location
            var saveLocationLine = lines.IndexOf(lines.First(l => l.Contains("SaveLocation")));
            var equalsPos = lines[saveLocationLine].IndexOf('=');
            lines[saveLocationLine] = lines[saveLocationLine].Substring(0, equalsPos) + $" = \"{settings.GoalsExportLocation.Replace("\\", "\\\\")}\";";

            // Update all the user information - First Name
            var firstNameLine = lines.IndexOf(lines.First(l => l.Contains("FirstName")));
            equalsPos = lines[firstNameLine].IndexOf('=');
            lines[firstNameLine] = lines[firstNameLine].Substring(0, equalsPos) + $" = \"{settings.FirstName}\";";

            // Update all the user information - last Name
            var lastNameLine = lines.IndexOf(lines.First(l => l.Contains("LastName")));
            equalsPos = lines[lastNameLine].IndexOf('=');
            lines[lastNameLine] = lines[lastNameLine].Substring(0, equalsPos) + $" = \"{settings.LastName}\";";

            // Write all the job entries into the export code file.
            for (int i = 0; i < jobEntries.Count(); i++)
            {
                var entry = jobEntries[i];
                lines.Insert(startIndex + i, $"jobList.Add(new JobRecord(" +
                    $"\"{entry.Group}\"," +
                    $"\"{entry.Job}\"," +
                    $"\"{entry.Ticket}\"," +
                    $"\"{entry.Description}\"," +
                    $"new DateTime({entry.Start.Ticks})," +
                    $"new DateTime({entry.End.Ticks})));");
            }

            // Turn the resultant lines into the complete export code.
            var stringBuilder = new StringBuilder();
            foreach (var line in lines)
            {
                stringBuilder.AppendLine(line);
            }

            return stringBuilder.ToString();
        }
    }
}
