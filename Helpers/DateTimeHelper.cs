﻿using System;

namespace Timepiece_V2.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime GetDateTime(DateTime defaultTime, DateTime date, string inputString)
        {
            var splitTime = inputString.Split(':');
            if (splitTime.Length != 2)
            {
                return date.AddHours(defaultTime.Hour).AddMinutes(defaultTime.Minute);
            }

            if (int.TryParse(splitTime[0], out var hours) && int.TryParse(splitTime[1], out var minutes))
            {
                return new DateTime(date.Year, date.Month, date.Day).AddHours(hours).AddMinutes(minutes);
            }

            return date.AddHours(defaultTime.Hour).AddMinutes(defaultTime.Minute); ;
        }


        public static DateTime? GetTime(string inputString)
        {
            var splitTime = inputString.Split(':');
            if (splitTime.Length != 2)
            {
                return null;
            }

            if (int.TryParse(splitTime[0], out var hours) && int.TryParse(splitTime[1], out var minutes))
            {
                return DateTime.MinValue.AddHours(hours).AddMinutes(minutes);
            }

            return null;
        }

        public static TimeSpan? GetDuration(string inputString)
        {
            var split = inputString.Split('h');
            
            if(split.Length != 2)
            {
                return null;
            }

            var minuteString = split[1].Split('m');

            if(int.TryParse(split[0], out var hours) && int.TryParse(minuteString[0], out var minutes))
            {
                return new TimeSpan(hours, minutes, 0);
            }

            return null;
        }
    }
}
