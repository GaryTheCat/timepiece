﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Timepiece_V2.Helpers
{
    public static class RecordHelper
    {
        private static JsonSerializerSettings SerialiserSettings => new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public static List<TimeEntry> LoadTimeEntries(string fileLocation)
        {
            var timeEntries = new List<TimeEntry>();

            if (File.Exists(fileLocation))
            {
                var data = File.ReadAllText(fileLocation);

                var splitEntries = data.Split('\n');

                foreach (var entry in splitEntries)
                {
                    if (entry == splitEntries.Last())
                    {
                        continue;
                    }

                    var timeEntry = JsonConvert.DeserializeObject<TimeEntry>(entry, SerialiserSettings);
                    if (timeEntry.Id == default)
                    {
                        timeEntry.Id = Guid.NewGuid();
                    }

                    timeEntries.Add(timeEntry);
                }
            }

            return timeEntries;
        }

        public static void UpdateRecords(string fileLocation, List<TimeEntry> currentRecords)
        {
            // Get existing time entries
            var fileTimeEntries = LoadTimeEntries(fileLocation);

            // Append the current time entries
            foreach (var entry in currentRecords)
            {
                var fileEntry = fileTimeEntries.FirstOrDefault(e => e.Id.Equals(entry.Id));

                if (fileEntry == null)
                {
                    // If this entry does not exist.
                    fileTimeEntries.Add(entry);
                }
                else
                {
                    // Update the time entry as it may have been modified.
                    fileTimeEntries.RemoveAt(fileTimeEntries.IndexOf(fileEntry));
                    fileTimeEntries.Add(entry);
                }
            }

            WriteEntriesToFile(fileLocation, fileTimeEntries);
        }

        private static void WriteEntriesToFile(string fileLocation, List<TimeEntry> entriesToWrite)
        {
            // Create a stream to serialize the object to.
            var objects = entriesToWrite.Select(c => JsonConvert.SerializeObject(c, SerialiserSettings));

            string file = string.Empty;

            foreach (var o in objects)
            {
                file += o.Trim() + '\n';
            }

            // Write that sucker to file.
            File.WriteAllText(fileLocation, file);
        }

        public static void RemoveRecords(string fileLocation, List<TimeEntry> recordsToRemove)
        {
            var records = LoadTimeEntries(fileLocation);
            var recordIds = recordsToRemove.Select(r => r.Id).ToList();

            foreach(var record in records.ToList())
            {
                if (recordIds.Contains(record.Id))
                {
                    records.Remove(record);
                }
            }

            WriteEntriesToFile(fileLocation, records);
        }
    }
}
