﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timepiece_V2.DataStructures;

namespace Timepiece_V2.Helpers
{
    public static class EventHelper
    {
        public static Tuple<DateTime, Event> GetNextEvent(List<Event> events)
        {
            Event earliestEvent = new Event
            {
                Start = DateTime.MaxValue
            };

            var occurances = GetAllFirstOccurances(events).OrderBy(o => o.Item1);

            return new Tuple<DateTime, Event>(occurances.First().Item1, occurances.First().Item2);
        }

        private static List<Tuple<DateTime, Event>> GetAllFirstOccurances(List<Event> events)
        {
            var eventOccurances = new List<Tuple<DateTime, Event>>();
            foreach (var e in events)
            {
                eventOccurances.Add(new Tuple<DateTime, Event>(GetFirstOccurance(e), e));
            }

            return eventOccurances;
        }

        public static DateTime GetFirstOccurance(Event e)
        {
            var firstOccuranceEver = e.NextOccurance;
            var currentTime = firstOccuranceEver;
            var eventDays = e.Days.Select(d => (int)d).ToList();
            var lastDayofWeek = eventDays.Max();

            if((int)currentTime.DayOfWeek > lastDayofWeek)
            {
                // Move us to the first day of the next week
                currentTime = currentTime.AddDays(-(int)currentTime.DayOfWeek);
                if(e.Reoccurance == Reoccurance.Daily || e.Reoccurance == Reoccurance.Weekly)
                {
                    currentTime = currentTime.AddDays(7);
                }

                if(e.Reoccurance == Reoccurance.Fortnightly)
                {
                    currentTime = currentTime.AddDays(14);
                }

                if(e.Reoccurance == Reoccurance.Triweekly)
                {
                    currentTime = currentTime.AddDays(21);
                }

                currentTime = currentTime.AddDays(eventDays.Min());
            }
            else if (!eventDays.Contains((int)currentTime.DayOfWeek))
            {
                for(int i = 0; i < eventDays.Count; i++)
                {
                    if((int)currentTime.DayOfWeek < eventDays[i])
                    {
                        currentTime = currentTime.AddDays(eventDays[i] - (int)currentTime.DayOfWeek);
                        break;
                    }
                }
            }

            while(currentTime < DateTime.Now)
            {
                while ((int)currentTime.DayOfWeek != lastDayofWeek)
                {
                    var currentDayOfWeek = (int)currentTime.DayOfWeek;
                    var nextDay = eventDays[eventDays.IndexOf(currentDayOfWeek) + 1];
                    var daysToSkip = nextDay - currentDayOfWeek;

                    currentTime = currentTime.AddDays(daysToSkip);

                    if(currentTime > DateTime.Now)
                    {
                        break;
                    }
                }

                if ((int)currentTime.DayOfWeek == lastDayofWeek)
                {
                    // Move us to the first day of the next week
                    currentTime = currentTime.AddDays(-(int)currentTime.DayOfWeek);
                    if (e.Reoccurance == Reoccurance.Daily || e.Reoccurance == Reoccurance.Weekly)
                    {
                        currentTime = currentTime.AddDays(7);
                    }

                    if (e.Reoccurance == Reoccurance.Fortnightly)
                    {
                        currentTime = currentTime.AddDays(14);
                    }

                    if (e.Reoccurance == Reoccurance.Triweekly)
                    {
                        currentTime = currentTime.AddDays(21);
                    }

                    currentTime = currentTime.AddDays(eventDays.Min());
                }
            }            

            return currentTime;
        }

        public static bool AreInsideEvent(Event e)
        {
            var now = DateTime.Now;
            return (e.NextOccurance < now && e.NextOccurance.AddMinutes(e.Duration) > now);
        }

        private static DateTime GetStartOfNextWeek(DateTime currentTime, Reoccurance occurance)
        {
            var currentSunday = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day).AddDays(-(int)currentTime.DayOfWeek);

            if(occurance == Reoccurance.Weekly)
            {
                return currentSunday.AddDays(7);
            }
            if(occurance == Reoccurance.Fortnightly)
            {
                return currentSunday.AddDays(14);
            }
            if(occurance == Reoccurance.Triweekly)
            {
                return currentSunday.AddDays(21);
            }

            return currentSunday;
        }
    }
}
