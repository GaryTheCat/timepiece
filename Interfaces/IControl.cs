﻿using System.Windows.Controls;

namespace Timepiece_V2.Interfaces
{
    public interface IControl
    {
        string Id { get; }

        bool IsSelected { get; set; }

        UserControl GetContent();
    }
}
