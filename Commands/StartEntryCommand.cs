﻿using System;
using System.Runtime.Serialization;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;

namespace Timepiece_V2.Commands
{
    [DataContract]
    public class StartEntryCommand : UndoRedoCommand
    {
        public StartEntryCommand(Guid entryId, long startTicks, string job, string ticket, string description, string group, string colour)
        {
            CommandId = Guid.NewGuid();
            Start = new DateTime(startTicks);
            End = Start;
            Job = job;
            Ticket = ticket;
            Description = description;
            Group = group;
            Colour = colour;
            EntryId = entryId;
        }

        [DataMember]
        public Guid EntryId { get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public DateTime End { get; set; }

        [DataMember]
        public string Job { get; set; }

        [DataMember]
        public string Ticket { get; set; }

        [DataMember]
        public string Description { get; set; } = string.Empty;

        [DataMember]
        public string Group { get; set; }

        [DataMember]
        public string Colour { get; set; } = "#3D8EF5";

        public override UndoRedoCommand Deserialise()
        {
            return null;
        }

        public override void Execute()
        {
            var entryManager = WindowService.GetWindow<EntryManager>();
            if(entryManager != null)
            {
                entryManager.StartNewTimeEntry(
                    this.CommandId,
                    this.EntryId,
                    this.Start,
                    this.End,
                    this.Job,
                    this.Ticket,
                    this.Description,
                    this.Group,
                    this.Colour);
            } 
        }

        public override string Serialise()
        {
            return null;
        }

        public override void Undo()
        {
            var entryManager = WindowService.GetWindow<EntryManager>();
            if (entryManager != null)
            {
                entryManager.DeleteTimeEntry(EntryId);
            }
        }
    }
}
