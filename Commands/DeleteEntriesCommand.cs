﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Helpers;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;

namespace Timepiece_V2.Commands
{
    [DataContract]
    public class DeleteEntriesCommand : UndoRedoCommand
    {
        [DataMember]
        private List<TimeEntry> timeEntries = new List<TimeEntry>();

        public DeleteEntriesCommand(List<TimeEntry> timeEntries)
        {
            CommandId = Guid.NewGuid();
            this.timeEntries = timeEntries.ToList();
        }

        public override UndoRedoCommand Deserialise()
        {
            throw new NotImplementedException();
        }

        public override void Execute()
        {
            var entryManager = WindowService.GetWindow<EntryManager>();
            entryManager.DeleteSelectedTimeEntries(timeEntries);

        }

        public override string Serialise()
        {
            throw new NotImplementedException();
        }

        public override void Undo()
        {
            var entryManager = WindowService.GetWindow<EntryManager>();
            var settings = WindowService.GetWindow<Settings>();

            foreach (var entry in timeEntries)
            {
                entry.IsSelected = false;
            }

            entryManager.AddTimeEntries(timeEntries);

            var groupedEntries = timeEntries.GroupBy(e => SprintHelper.GetSprintName(settings.SprintStartDate, e.Start));

            foreach (var sprint in groupedEntries)
            {
                RecordHelper.RemoveRecords(
                    Configuration.GetBackupRecordsFileLocation(sprint.Key), 
                    sprint.Select(e => e).ToList());
            }
        }
    }
}