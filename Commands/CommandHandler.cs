﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Timepiece_V2.Commands
{
    public class CommandHandler
    {
        private List<UndoRedoCommand> redoStack;

        private List<UndoRedoCommand> undoStack;

        private bool newCommandsToWrite = false;

        private Timer writeHistoryTimer;

        private JsonSerializerSettings SerialiserSettings => new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public CommandHandler()
        {
            undoStack = new List<UndoRedoCommand>();

            redoStack = new List<UndoRedoCommand>();

            //writeHistoryTimer = new Timer(WriteHistory, null, 10000, 10000);
        }

        public void TryLoadHistory()
        {
            // The file exists which indicates it wasn't deleted on close - Likely due to a crash
            // so we should re-apply all of the commands.
            if (File.Exists(Configuration.CommandsFile))
            {
                var data = File.ReadAllText(Configuration.CommandsFile);

                var splitEntries = data.Split('\n');
                
                foreach (var entry in splitEntries)
                {
                    if(entry == splitEntries.Last())
                    {
                        return;
                    }

                    var command = JsonConvert.DeserializeObject<UndoRedoCommand>(entry, SerialiserSettings);
                    
                    Execute(command);
                }

                newCommandsToWrite = false;
            }
        }

        private void WriteHistory(object _)
        {
            if (newCommandsToWrite)
            {
                newCommandsToWrite = false;

                File.WriteAllText(Configuration.CommandsFile, GetSerialisedCommands(undoStack));
            }
        }

        public void Execute(UndoRedoCommand command)
        {
            redoStack.Clear();
            newCommandsToWrite = true;

            command.Execute();
            undoStack.Add(command);
        }

        public void Undo()
        {
            var previousCommand = undoStack.LastOrDefault();
            if(previousCommand != null)
            {
                previousCommand.Undo();
                undoStack.Remove(previousCommand);
                redoStack.Add(previousCommand);
            }
        }

        public void Redo()
        {
            var command = redoStack.LastOrDefault();
            if (command != null)
            {
                command.Execute();
                redoStack.Remove(command);
                undoStack.Add(command);
            }
        }

        public UndoRedoCommand GetCommand(Guid id)
        {
            return undoStack.FirstOrDefault(c => c.CommandId.Equals(id));
        }

        private string GetSerialisedCommands(List<UndoRedoCommand> commands)
        {
            var objects = commands.Select(c => JsonConvert.SerializeObject(c, SerialiserSettings));
            string file = string.Empty;

            foreach (var o in objects)
            {
                file += o.Trim() + '\n';
            }

            return file;
        }

        public void ReplaceUndoCommand(Guid commandId, UndoRedoCommand newCommand)
        {
            var command = undoStack.FirstOrDefault(c => c.CommandId.Equals(commandId));
            if(command == null)
            {
                return;
            }

            var index = undoStack.IndexOf(command);
            undoStack.Remove(command);

            undoStack.Insert(index, newCommand);
        }
    }
}