﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Timepiece_V2.Commands
{
    [DataContract]
    public abstract class UndoRedoCommand
    {
        [DataMember]
        public Guid CommandId { get; set; }

        public abstract void Undo();

        public abstract void Execute();

        public abstract string Serialise();

        public abstract UndoRedoCommand Deserialise();
    }
}