﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Timepiece_V2.Dialogs.YesNoDialog;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.ThisSprint;
using Timepiece_V2.UIControls.TimeEntryContextMenu;
using Timepiece_V2.Workflows;
using Timepiece_V2.Workflows.Double;
using Timepiece_V2.Workflows.String;

namespace Timepiece_V2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel { get; set; }

        public MainWindow()
        {
            this.ViewModel = new MainWindowViewModel();
            this.DataContext = ViewModel;

            if (File.Exists(Configuration.TempFile))
            {
                var dialog = new YesNoDialogViewModel("There is a lock file indicating you have timepiece open already. Do you want to continue?");

                if (!dialog.Show())
                {
                    Close();
                }
            }

            File.Create(Configuration.TempFile);

            InitializeComponent();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ProcessClearClick();
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ProcessExportClick();
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ProcessSettingsClick();
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            if (!ViewModel.IsSettingsClosed)
            {
                ViewModel.SettingsWindowView.Close();
            }

            this.ViewModel.Save();

            if (File.Exists(Configuration.CommandsFile))
            {
                File.Delete(Configuration.CommandsFile);
            }

            if (File.Exists(Configuration.TempFile))
            {
                File.Delete(Configuration.TempFile);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var settings = this.ViewModel.SettingsWindowViewModel.Settings;

            if (ViewModel.FirstLoad)
            {
                this.ViewModel.WorkflowManagerViewModel = new WorkflowManagerViewModel();

                // Placeholder until this is completely implemented
                var firstNamePage = new WorkflowPageStringViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "What is your first name?",
                        "fName");

                var lastNamePage = new WorkflowPageStringViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "What is your last name?",
                        "lName");

                var workHoursPage = new WorkflowPageDoubleViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "How many hours do you work per sprint?",
                        120);

                var ticketPrefixPage = new WorkflowPageStringViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "What is your ticket prefix?",
                        "EVO-");

                var sprintStartDatePage = new WorkflowPageStringViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "When was the start of your last sprint?",
                        "28/10/2019");

                var exportLocationPage = new WorkflowPageStringViewModel(
                        this.ViewModel.WorkflowManagerViewModel,
                        "Where would you like to export your goals file?",
                        "G:\\");

                firstNamePage.WriteValue = () => settings.FirstName = firstNamePage.Answer;
                lastNamePage.WriteValue = () => settings.LastName = lastNamePage.Answer;
                workHoursPage.WriteValue = () => settings.HoursInSprint = workHoursPage.Answer;
                ticketPrefixPage.WriteValue = () => settings.TicketPrefix = ticketPrefixPage.Answer;
                sprintStartDatePage.WriteValue = () => settings.SprintStartDate = Convert.ToDateTime(sprintStartDatePage.Answer);
                exportLocationPage.WriteValue = () => settings.GoalsExportLocation = exportLocationPage.Answer;
                
                this.ViewModel.WorkflowManagerViewModel.AddPage(firstNamePage);
                this.ViewModel.WorkflowManagerViewModel.AddPage(lastNamePage);
                this.ViewModel.WorkflowManagerViewModel.AddPage(workHoursPage);
                this.ViewModel.WorkflowManagerViewModel.AddPage(ticketPrefixPage);
                this.ViewModel.WorkflowManagerViewModel.AddPage(sprintStartDatePage);
                this.ViewModel.WorkflowManagerViewModel.AddPage(exportLocationPage);

                this.ViewModel.WorkflowManagerViewModel.ShowWorkflow(this);
            }

            this.ViewModel.View = this;


            if (settings.IsMaximised)
            {
                this.WindowState = WindowState.Maximized;
            }
            else if (settings.WindowWidth != 0)
            {
                this.Width = settings.WindowWidth;
                this.Height = settings.WindowHeight;
            }

        }

        private void RepeatEvents_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ProcessRepeatEventsClick();
        }

        private void AddSickLeave_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ProcessSickLeaveClick();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs args)
        {
            if (Keyboard.IsKeyDown(Key.Delete) && this.ViewModel.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.Any(e => e.IsSelected))
            {
                this.ViewModel.ProcessClearClick();
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.Z))
            {
                this.ViewModel.CommandHandler.Undo();
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.Y))
            {
                this.ViewModel.CommandHandler.Redo();
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.A))
            {
                var entries = ViewModel.EntryManager.GetTimeEntries();
                bool visible = entries.Any(e => !e.IsSelected);
                foreach(var entry in entries)
                {
                    entry.IsSelected = visible;
                }

                MessageBus.Send<EntrySelectionChangedEvent>();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ViewModel.LeftHandPane.GetControlModel<TicketInputViewModel>().Timer.Dispose();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var TimeEntryContextMenu = WindowService.GetWindow<TimeEntryContextMenuViewModel>();
                TimeEntryContextMenu.Visible = Visibility.Collapsed;

                if (this.WindowState == WindowState.Maximized)
                {
                    this.WindowState = WindowState.Normal;
                }

                try
                {
                    this.DragMove();
                }
                catch { }
            }
        }

        private void Window_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            int tolerance = 20000;

            MousePositionManager.UpdateMousePosition(
                MyWindow.ActualWidth, 
                MyWindow.ActualHeight, 
                Mouse.GetPosition(MyWindow));

            var distanceFromTopRight = MousePositionManager.GetDistanceFromTopRight(tolerance);
            var distanceFromLeft = MousePositionManager.GetDistanceFromLeft(tolerance);
            var distanceFromRight = MousePositionManager.GetDistanceFromRight(tolerance);

            if (distanceFromTopRight < tolerance)
            {
                ToolbarButtons.Opacity = Math.Min((tolerance - distanceFromTopRight) / 12000, 1);
            }
            else
            {
                ToolbarButtons.Opacity = 0;
            }

            if (!ViewModel.WindowState.IsLeftPaneOpen)
            {
                if (distanceFromLeft < 9000)
                {
                    ViewModel.WindowState.OpenLeftPaneOpacity = Math.Min((9000 - distanceFromLeft) / 7000, 1);
                }
                else
                {
                    ViewModel.WindowState.OpenLeftPaneOpacity = 0;
                }
            }

            if (!ViewModel.WindowState.IsRightPaneOpen)
            {
                if (distanceFromRight < 9000)
                {
                    ViewModel.WindowState.OpenRightPaneOpacity = Math.Min((9000 - distanceFromRight) / 7000, 1);
                }
                else
                {
                    ViewModel.WindowState.OpenRightPaneOpacity = 0;
                }
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Maximise_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState.Equals(WindowState.Normal))
            {
                this.WindowState = WindowState.Maximized;
            }
            else
            {
                this.WindowState = WindowState.Normal;
            }
        }

        private void Minimise_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void MaximiseLeftPane_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.MaximiseLeftPane();
        }

        private void MaximiseRightPane_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.WindowStateManager.MaximiseRightPane();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}