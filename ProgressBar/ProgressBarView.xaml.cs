﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Timepiece_V2.ProgressBar
{
    /// <summary>
    /// Interaction logic for ProgressBarView.xaml
    /// </summary>
    public partial class ProgressBarView : UserControl
    {
        public ProgressBarViewModel ViewModel => this.DataContext as ProgressBarViewModel;

        public ProgressBarView()
        {
            InitializeComponent();
        }

        private void ProgressBar_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.Progress = Progress;
        }
    }
}
