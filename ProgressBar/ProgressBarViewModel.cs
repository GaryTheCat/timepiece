﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Timepiece_V2.Enums;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.Systems;

namespace Timepiece_V2.ProgressBar
{
    public class ProgressBarViewModel : ReactiveViewModelBase
    {
        public double BarWidth = 250;

        private double LeftBarPercentage = 0;

        private double RightBarPercentage => 1 - LeftBarPercentage;

        public double LeftBarSize => LeftBarPercentage * BarWidth;

        public double RightBarSize => RightBarPercentage * BarWidth;

        public Color ProgressGreenLight => Color.FromRgb(0x81, 0xE1, 0x9A);

        public Color ProgressGreen => Color.FromRgb(0x81, 0xAB, 0xE1);

        public Grid Progress { get; set; }

        private DoubleAnimator progressAnimator;

        public ProgressBarViewModel()
        {

        }

        public void SetProgress(double newProgress)
        {
            if(newProgress < 0 || newProgress > 1)
            {
                throw new Exception("Progress cannot be outside of the range 0 -> 1.");
            }

            LeftBarPercentage = newProgress;
            OnPropertyChanged("LeftBarSize");
        }

        public void SetProgress(double newProgress, AnimationType type)
        {
            Progress.StartAnimation(LeftBarSize, newProgress * BarWidth, 0.3, Grid.WidthProperty, () => { });
            Progress.StartAnimation(ProgressGreenLight, ProgressGreen, 0.3);
            LeftBarPercentage = newProgress;
        }
    }
}
