﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Timepiece_V2.ExtensionMethods
{
    public static class FrameworkElementExtensions
    {
        public static void StartAnimation(
            this FrameworkElement element,
            double from,
            double to,
            double durationSeconds,
            DependencyProperty property,
            Action callback,
            bool force = false)
        {
            // If we are already at our 'to' position, dont re-start the animation.
            var currentValue = (double)element.GetValue(property);
            if(currentValue == to && !force)
            {
                return;
            }

            // Create the animation.
            var animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new Duration(TimeSpan.FromSeconds(durationSeconds));

            // Set the element for the animation.
            Storyboard.SetTarget(animation, element);
            Storyboard.SetTargetProperty(animation, new PropertyPath(property));

            // Create and add the animation to the storyboard.
            var storyboard = new Storyboard();
            storyboard.Children.Add(animation);

            // Trigger the completed callback
            storyboard.Completed += (s, a) => callback();

            // Begin the storyboard animations.
            storyboard.Begin();
        }

        public static void StartAnimation(
           this Border element,
           Color from,
           Color to,
           double durationSeconds,
           bool border = false)
        {
            // Create the animation.
            var animation = new ColorAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new Duration(TimeSpan.FromSeconds(durationSeconds));

            if (border)
            {
                element.BorderBrush = new SolidColorBrush(from);
                element.BorderBrush.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            }
            else
            {
                element.Background = new SolidColorBrush(from);
                element.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            }
        }

        public static void StartAnimation(
           this Grid element,
           Color from,
           Color to,
           double durationSeconds)
        {
            // Create the animation.
            var animation = new ColorAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new Duration(TimeSpan.FromSeconds(durationSeconds));

            element.Background = new SolidColorBrush(from);
            element.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);
        }
    }
}
