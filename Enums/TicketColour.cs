﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timepiece_V2.Enums
{
    public enum TicketColour
    {
        Blue,
        Green,
        Yellow,
        Orange,
        Red,
        Pink
    }
}
