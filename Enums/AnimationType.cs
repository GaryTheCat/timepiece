﻿namespace Timepiece_V2.Enums
{
    public enum AnimationType
    {
        FadeOutIn,
        Highlight,
        Transform
    }
}
