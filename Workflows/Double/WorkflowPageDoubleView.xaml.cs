﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Timepiece_V2.Workflows.Double
{
    /// <summary>
    /// Interaction logic for WorkflowPageDoubleView.xaml
    /// </summary>
    public partial class WorkflowPageDoubleView : UserControl
    {
        public WorkflowPageDoubleViewModel ViewModel => this.DataContext as WorkflowPageDoubleViewModel;

        public WorkflowPageDoubleView()
        {
            InitializeComponent();
        }
        
        private void AnswerBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox.Text.Equals(ViewModel.DefaultAnswer))
            {
                textBox.Text = "0";
            }
        }

        private void AnswerBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = ViewModel.DefaultAnswer.ToString();
            }
        }

        private void AnswerBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter))
            {
                ViewModel.WorkflowManagerViewModel.ShowNextPage();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.FirstField = AnswerTextBox;
        }
    }
}
