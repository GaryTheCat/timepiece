﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Timepiece_V2.Workflows.Double
{
    public class WorkflowPageDoubleViewModel : WorkflowPageViewModel
    {
        public string Question { get; set; }

        public double Answer { get; set; }

        public double DefaultAnswer { get; set; }

        public string AnswerTextColour => Answer.Equals(DefaultAnswer) ? "#aeaeae" : "#4e4e4e";

        public TextBox FirstField { get; set; }


        public WorkflowPageDoubleViewModel(
            WorkflowManagerViewModel managerViewModel,
            string question,
            double defaultAnswer) : base(managerViewModel)
        {
            Question = question;

            DefaultAnswer = defaultAnswer;

            Answer = DefaultAnswer;

            Content = new WorkflowPageDoubleView();

            Content.DataContext = this;

            SetFirstFieldFocus = () => { FirstField.Focus(); };
        }
    }
}
