﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.ProgressBar;

namespace Timepiece_V2.Workflows
{
    public class WorkflowManagerViewModel : ReactiveViewModelBase
    {
        public WorkflowManagerViewModel()
        {
            pages = new List<WorkflowPageViewModel>();

            ProgressBarViewModel = new ProgressBarViewModel();
        }

        public ProgressBarViewModel ProgressBarViewModel { get; set; }

        public void AddPage(WorkflowPageViewModel page)
        {
            pages.Add(page);
        }

        public void ShowWorkflow(Window owner)
        {
            var workflowView = new WorkflowManagerView();
            workflowView.Owner = owner;
            workflowView.DataContext = this;

            selectedPage = pages.FirstOrDefault();

            workflowView.Show();
        }

        public bool ShowNextPage()
        {
            selectedPageIndex++;

            if (pages.Count > selectedPageIndex)
            {
                WorkflowPage.StartAnimation(1, 0, 0.3, UIElement.OpacityProperty, () =>
                {

                    selectedPage = pages[selectedPageIndex];

                    UpdateProgress();

                    WorkflowPage.StartAnimation(0, 1, 0.3, UIElement.OpacityProperty, () => { selectedPage.SetFirstFieldFocus(); });
                });

                return true;
            }
            else
            {
                UpdateProgress();

                // Start close procedure
                View.StartAnimation(1, 0, 0.3, UIElement.OpacityProperty, () =>
                {
                    foreach(var page in this.pages)
                    {
                        page.WriteValue();
                    }

                    View.Close();
                });
                return false;
            }
        }

        public bool ShowPreviousPage()
        {
            selectedPageIndex--;

            if (selectedPageIndex >= 0)
            {
                selectedPage = pages[selectedPageIndex];

                WorkflowPage.StartAnimation(1, 0, 0.3, UIElement.OpacityProperty, () =>
                {
                    selectedPage = pages[selectedPageIndex];

                    UpdateProgress();

                    WorkflowPage.StartAnimation(0, 1, 0.3, UIElement.OpacityProperty, () => { selectedPage.SetFirstFieldFocus(); });
                });
                
                return true;
            }
            else
            {
                selectedPageIndex++;
                return false;
            }
        }

        private void UpdateProgress()
        {
            var progress = selectedPageIndex * 1.0 / pages.Count();
            ProgressBarViewModel.SetProgress(progress, Enums.AnimationType.Transform);
            OnPropertyChanged("SelectedContent");
            OnPropertyChanged("NextImageSource");
            OnPropertyChanged("IsBackVisibile");
        }

        private int selectedPageIndex = 0;

        private WorkflowPageViewModel selectedPage;

        private List<WorkflowPageViewModel> pages;

        public UserControl SelectedContent => selectedPage.Content;

        public string NextImageSource => selectedPageIndex < pages.Count() - 1 ? @"../Images/Next.png" : @"../Images/Tick.png";

        public bool IsBackVisibile => selectedPageIndex != 0;

        public Window View { get; set; }
        public ContentControl WorkflowPage { get; set; }
    }
}
