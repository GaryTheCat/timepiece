﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Timepiece_V2.Workflows.String
{
    /// <summary>
    /// Interaction logic for WorkflowPageStringView.xaml
    /// </summary>
    public partial class WorkflowPageStringView : UserControl
    {
        public WorkflowPageStringViewModel ViewModel => this.DataContext as WorkflowPageStringViewModel;

        public WorkflowPageStringView()
        {
            InitializeComponent();
        }

        private void AnswerBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox.Text.Equals(ViewModel.DefaultAnswer))
            {
                textBox.Text = string.Empty;
            }
        }

        private void AnswerBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = ViewModel.DefaultAnswer;
            }
        }

        private void AnswerBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter))
            {
                ViewModel.WorkflowManagerViewModel.ShowNextPage();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.FirstField = AnswerTextBox;
        }
    }
}
