﻿using System.Windows.Controls;

namespace Timepiece_V2.Workflows.String
{
    public class WorkflowPageStringViewModel : WorkflowPageViewModel
    {
        public string Question { get; set; }

        public string Answer { get; set; }

        public string DefaultAnswer { get; set; }

        public string AnswerTextColour => Answer .Equals(DefaultAnswer) ? "#aeaeae" : "#4e4e4e";

        public TextBox FirstField { get; set; }

        public WorkflowPageStringViewModel(
            WorkflowManagerViewModel managerViewModel,
            string question, 
            string defaultAnswer) : base(managerViewModel)
        {
            Question = question;

            DefaultAnswer = defaultAnswer;

            Answer = DefaultAnswer;

            Content = new WorkflowPageStringView();

            Content.DataContext = this;

            SetFirstFieldFocus = () => { FirstField.Focus(); };
        }
    }
}
