﻿using System;
using System.Windows.Controls;

namespace Timepiece_V2.Workflows
{
    public class WorkflowPageViewModel : ReactiveViewModelBase
    {
        public WorkflowPageViewModel(WorkflowManagerViewModel managerViewModel)
        {
            WorkflowManagerViewModel = managerViewModel;
        }

        public UserControl Content { get; set; }

        public WorkflowManagerViewModel WorkflowManagerViewModel;

        public Action SetFirstFieldFocus { get; set; } = () => { };

        public Action WriteValue { get; set; } = () => { };
    }
}
