﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Timepiece_V2.Workflows
{
    /// <summary>
    /// Interaction logic for WorkflowManagerView.xaml
    /// </summary>
    public partial class WorkflowManagerView : Window
    {
        public WorkflowManagerViewModel ViewModel => this.DataContext as WorkflowManagerViewModel;

        public WorkflowManagerView()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                try
                {
                    this.DragMove();
                }
                catch { }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.View = this;
            ViewModel.WorkflowPage = WorkflowPage;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ShowPreviousPage();
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ShowNextPage();
        }
    }
}
