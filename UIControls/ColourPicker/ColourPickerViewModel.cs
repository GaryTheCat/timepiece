﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Shapes;
using Timepiece_V2.Enums;

namespace Timepiece_V2.UIControls.ColourPicker
{
    public class ColourPickerViewModel
    {
        public List<Ellipse> Colours { get; set; }

        public ColourPickerView View { get; set; }

        public string SelectedColour { get; set; } = "#FF3D8EF5";

        public Dictionary<Ellipse, string> ColourMap { get; set; }
    }
}
