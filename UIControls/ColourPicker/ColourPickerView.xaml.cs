﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Shapes;
using Timepiece_V2.Enums;
using Timepiece_V2.ExtensionMethods;

namespace Timepiece_V2.UIControls.ColourPicker
{
    /// <summary>
    /// Interaction logic for ColourPickerView.xaml
    /// </summary>
    public partial class ColourPickerView : System.Windows.Controls.UserControl
    {
        public ColourPickerViewModel ViewModel => (ColourPickerViewModel)this.DataContext;

        public ColourPickerView()
        {
            InitializeComponent();
        }
        
        private void Ellipse_MouseLeave(object sender, MouseEventArgs e)
        {
            var ellipse = (Ellipse)sender;

            if(ellipse.StrokeThickness == 0)
            {
                ellipse.StartAnimation(16, 14, 0, WidthProperty, () => { });
                ellipse.StartAnimation(16, 14, 0, HeightProperty, () => { });
            }
        }

        private void Ellipse_MouseEnter(object sender, MouseEventArgs e)
        {
            var ellipse = (Ellipse)sender;
            if (ellipse.ActualWidth > 14 || ellipse.ActualHeight > 14)
            {
                return;
            }

            ellipse.StartAnimation(14, 16, 0, WidthProperty, () => { });
            ellipse.StartAnimation(14, 16, 0, HeightProperty, () => { });
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Colours = new List<Ellipse>()
            {
                BlueEllipse,
                GreenEllipse,
                YellowEllipse,
                OrangeEllipse,
                RedEllipse,
                PinkEllipse
            };

            ViewModel.ColourMap = new Dictionary<Ellipse, string>()
            {
                { BlueEllipse, "#FF3D8EF5" },
                { GreenEllipse, "#FF2FC957" },
                { YellowEllipse, "#FFF5AC3C" },
                { OrangeEllipse, "#FFF6633C" },
                { RedEllipse, "#FFF03A3A" },
                { PinkEllipse, "#FFEA5AC9" },
            };

            ViewModel.View = this;

            SetSelection(ViewModel.SelectedColour);
        }

        public void SetSelection(string colour)
        {
            var ellipse = ViewModel.ColourMap.First(m => m.Value.Equals(colour)).Key;

            Ellipse_MouseDown(ellipse, null);
        }

        public void Ellipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e != null)
            {
                e.Handled = true;
            }

            var ellipse = (Ellipse)sender;

            if (ellipse.ActualWidth == 18 || ellipse.ActualHeight == 18)
            {
                return;
            }

            foreach (var colour in ViewModel.Colours.Where(c => c.StrokeThickness != 0))
            {
                colour.StartAnimation(1.5, 0, 0.1, Shape.StrokeThicknessProperty, () => { });
                colour.StartAnimation(18, 14, 0.1, WidthProperty, () => { });
                colour.StartAnimation(18, 14, 0.1, HeightProperty, () => { });
            }

            ellipse.StartAnimation(14, 18, 0.1, WidthProperty, () => { });
            ellipse.StartAnimation(14, 18, 0.1, HeightProperty, () => { });
            ellipse.StartAnimation(0, 1.5, 0.1, Shape.StrokeThicknessProperty, () => { });

            ViewModel.SelectedColour = ViewModel.ColourMap[ellipse];
        }
    }
}
