﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.UIControls.ThisSprint;

namespace Timepiece_V2
{
    /// <summary>
    /// Interaction logic for TicketInput.xaml
    /// </summary>
    public partial class TicketInput : UserControl
    {
        public TicketInputViewModel ViewModel => (TicketInputViewModel)this.DataContext;

        public SettingsWindowViewModel SettingsViewModel => this.ViewModel.SettingsViewModel;

        public TicketInput()
        {
            InitializeComponent();
        }

        private void Start_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!this.ViewModel.IsTimerRunning)
            {
                if (this.ViewModel.StartNew())
                {

                }
            }
            else
            {
                this.ViewModel.Stop();
            }
        }

        private void PlayStartAnimation()
        {
            StartButtonBorder.StartAnimation(
                    Color.FromRgb(0x3D, 0x8E, 0xF5),
                    Color.FromRgb(0xF0, 0x3A, 0x3A),
                    0.3);

            StartStopImage.StartAnimation(1, 0, 0.15, OpacityProperty, () =>
            {
                StartStopImage.Source = new BitmapImage(new Uri("../Images/Stop.png", UriKind.Relative));
                StartStopImage.StartAnimation(
                    0,
                    1,
                    0.15,
                    OpacityProperty,
                    () => { });
            });
        }

        private void PlayStopAnimation()
        {
            StartButtonBorder.StartAnimation(
                Color.FromRgb(0xF0, 0x3A, 0x3A),
                Color.FromRgb(0x3D, 0x8E, 0xF5),
                0.3);

            StartStopImage.StartAnimation(1, 0, 0.15, OpacityProperty, () =>
            {
                StartStopImage.Source = new BitmapImage(new Uri("../Images/Start.png", UriKind.Relative));
                StartStopImage.StartAnimation(
                    0,
                    1,
                    0.15,
                    OpacityProperty,
                    () => { });
            });
        }

        private void Stop_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.ViewModel.Stop();
        }

        private void JobNumber_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (JobComboBox.Text.Equals("Job"))
            {
                JobComboBox.Text = this.SettingsViewModel.Settings.JobPrefix;
            }

            var cmbTextBox = (TextBox)JobComboBox.Template.FindName("PART_EditableTextBox", JobComboBox);
            cmbTextBox.CaretIndex = JobComboBox.Text.Length;
        }

        private void JobNumber_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (JobComboBox.Text.Equals(this.SettingsViewModel.Settings.JobPrefix))
            {
                JobComboBox.Text = "Job";
            }
        }

        private void TicketNumber_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (TicketComboBox.Text.Equals("Ticket"))
            {
                TicketComboBox.Text = this.SettingsViewModel.Settings.TicketPrefix;
            }

            var cmbTextBox = (TextBox)TicketComboBox.Template.FindName("PART_EditableTextBox", TicketComboBox);
            cmbTextBox.CaretIndex = TicketComboBox.Text.Length;
        }

        private void TicketNumber_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (TicketComboBox.Text.Equals(this.SettingsViewModel.Settings.TicketPrefix))
            {
                TicketComboBox.Text = "Ticket";
            }
        }

        private void TextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            var textBox = ((TextBox)sender);
            if (textBox.Text.Equals("0"))
            {
                ((TextBox)sender).Text = string.Empty;
            }
        }

        private void TextBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            var textBox = ((TextBox)sender);
            if (string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "0";
            }
        }

        private void ComboBox_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var combobox = (ComboBox)sender;
            this.ViewModel.Group = combobox.Text;
        }

        private void PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(Keyboard.IsKeyDown(Key.Enter) || Keyboard.IsKeyDown(Key.Return))
            {
                this.Start_Click(null, null);
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (ViewModel.IsTimerRunning)
            {
                var currentlyRunning = ViewModel.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.First();
                currentlyRunning.Description = ((TextBox)sender).Text;
            }
        }

        private void Description_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (textBox.Text.Equals(ViewModel.DefaultDescription))
            {
                textBox.Text = string.Empty;
            }
        }

        private void Description_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = ViewModel.DefaultDescription;
            }
        }

        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.PlayStartAnimation = this.PlayStartAnimation;
            ViewModel.PlayStopAnimation = this.PlayStopAnimation;
        }

        private void Minimise_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.ViewModel.MainWindowViewModel.MinimiseLeftPane();
        }

        private void GroupComboBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (GroupComboBox.Text.Equals("Group"))
            {
                GroupComboBox.Text = string.Empty;
            }
        }

        private void GroupComboBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(GroupComboBox.Text))
            {
                GroupComboBox.Text = "Group";
            }
        }
    }
}