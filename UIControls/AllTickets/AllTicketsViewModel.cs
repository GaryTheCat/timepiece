﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Helpers;
using Timepiece_V2.Managers;

namespace Timepiece_V2.UIControls.AllTickets
{
    public class AllTicketsViewModel : ITab
    {
        public AllTicketsViewModel(EntryManager entryManager)
        {
            header = "Prev. Sprints";

            content = new AllTicketsContentView();

            content.DataContext = this;

            if (Directory.Exists(Configuration.BackupRecordsDirectory))
            {
                foreach (var file in Directory.GetFiles(Configuration.BackupRecordsDirectory).Reverse())
                {
                    var day = new DayRecords();

                    var name = Path.GetFileNameWithoutExtension(file);

                    day.DayName = name;
                    day.Collapsed = Visibility.Collapsed;

                    foreach(var record in RecordHelper.LoadTimeEntries(file))
                    {
                        day.TimeEntries.Add(record);
                    }

                    TimeEntries.Add(day);
                }
            }
        }

        public ObservableCollection<DayRecords> TimeEntries { get; set; }
            = new ObservableCollection<DayRecords>();

        private AllTicketsContentView content;

        private string header;

        public bool IsSelected { get; set; }

        public FrameworkElement Accent { get; set; }

        public FrameworkElement Header { get; set; }

        public string Id => "PreviousSprints";

        public UserControl GetContent()
        {
            return content;
        }

        public string GetHeaderText()
        {
            return header;
        }
    }
}
