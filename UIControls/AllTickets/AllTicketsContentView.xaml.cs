﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Timepiece_V2.DataStructures;

namespace Timepiece_V2.UIControls.AllTickets
{
    /// <summary>
    /// Interaction logic for AllTicketsContentView.xaml
    /// </summary>
    public partial class AllTicketsContentView : UserControl
    {
        public AllTicketsContentView()
        {
            InitializeComponent();
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var dayRecords = (sender as TextBlock).DataContext as DayRecords;
            dayRecords.Collapsed = dayRecords.Collapsed == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }
    }
}
