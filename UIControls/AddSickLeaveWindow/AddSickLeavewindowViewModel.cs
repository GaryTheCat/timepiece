﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timepiece_V2.UIControls.ThisSprint;

namespace Timepiece_V2.UIControls.AddSickLeaveWindow
{
    public class AddSickLeaveWindowViewModel : ReactiveViewModelBase
    {
        public string DisplayName => $"Add Leave";

        public AddSickLeaveWindowViewModel(MainWindowViewModel mainWindowViewModel)
        {
            this.MainWindowViewModel = mainWindowViewModel;

            ReasonsforDayOff = new List<string> { "Leave", "Sick" };

            SelectedDay = DateTime.Now;
        }
       
        public MainWindowViewModel MainWindowViewModel { get; set; }

        public List<string> ReasonsforDayOff { get; set; }
            
        public string SelectedReason { get; set; } = "Sick";

        public DateTime SelectedDay { get; set; }

        public void AddLeaveDay()
        {
            // TODO - Add this back in.
            //MainWindowViewModel.TabContentManagerViewModel.ThisSprintViewModel.AddNewTimeEntry(
            //      this.SelectedDay.AddHours(9),
            //      this.SelectedDay.AddHours(17),
            //      this.SelectedReason);

            SetNextMissingDay();
        }

        public void SetNextMissingDay()
        {
            // Take a guess at the day
            for (int i = -6; i < 0; i++)
            {
                var day = DateTime.Now.AddDays(i);
                if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }

                var key = new DateTime(day.Year, day.Month, day.Day);

                if (!MainWindowViewModel.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().Days.Any(e => e.Id == key))
                {
                    SelectedDay = key;
                    break;
                }
            }
        }
    }
}