﻿using System.Windows;

namespace Timepiece_V2.UIControls.AddSickLeaveWindow
{
    /// <summary>
    /// Interaction logic for AddSickLeaveWindowView.xaml
    /// </summary>
    public partial class AddSickLeaveWindowView : Window
    {
        public AddSickLeaveWindowViewModel ViewModel => (AddSickLeaveWindowViewModel)this.DataContext;

        public AddSickLeaveWindowView()
        {
            InitializeComponent();
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.AddLeaveDay();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SetNextMissingDay();
        }
    }
}
