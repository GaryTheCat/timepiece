﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Timepiece_V2.Commands;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Enums;
using Timepiece_V2.Interfaces;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.ColourPicker;
using Timepiece_V2.UIControls.ThisSprint;
using Timepiece_V2.UIControls.ViewConductor;

namespace Timepiece_V2
{
    public class TicketInputViewModel : ReactiveViewModelBase, IControl
    {
        public TicketInputViewModel(
            ToastManager toastManager,
            EntryManager entryManager,
            MainWindowViewModel mainWindowViewModel, 
            TabContentManagerViewModel TabContentManagerViewModel,
            SettingsWindowViewModel settingsViewModel,
            ReocurringEventManager eventManager,
            CommandHandler commandHandler,
            ConductorViewModel conductor)
        {
            this.ToastManager = toastManager;
            this.EntryManager = entryManager;
            this.MainWindowViewModel = mainWindowViewModel;
            this.TabContentManagerViewModel = TabContentManagerViewModel;
            this.SettingsViewModel = settingsViewModel;
            this.EventManager = eventManager;
            this.CommandHandler = commandHandler;
            this.Conductor = conductor;

            this.Timer = new Timer(UpdateTimer, null, 250, 50);
            JobDuration = new TimeSpan(0);

            PreviousJobs = new List<string>();
            PreviousTickets = new List<string>();
            PreviousGroups = new List<string>();

            content = new TicketInput();

            content.DataContext = this;

            ColourPickerViewModel = new ColourPickerViewModel();

            MessageBus.Listen<RunningEntryRemovedEvent>(() => Stop());
        }

        private UserControl content;

        public CommandHandler CommandHandler { get; set; }

        public ColourPickerViewModel ColourPickerViewModel { get; set; }

        public ConductorViewModel Conductor { get; set; }

        public SettingsWindowViewModel SettingsViewModel { get; set; }

        public MainWindowViewModel MainWindowViewModel { get; set; }

        public ReocurringEventManager EventManager { get; set; }

        public ToastManager ToastManager { get; set; }

        public EntryManager EntryManager { get; set; }

        public Action PlayStartAnimation { get; set; }

        public Action PlayStopAnimation { get; set; }

        public List<Ellipse> Colours { get; set; }

        public List<string> PreviousJobs { get; set; }

        public List<string> PreviousTickets { get; set; }

        public List<string> PreviousGroups { get; set; }

        public ObservableCollection<string> GroupItems { get; set; }
            = new ObservableCollection<string>();

        public string Group { get; set; } = "Group";

        public string TimerColour { get; set; } = "#4CAF50";

        public string StartStopSource => IsTimerRunning ? "Images/Stop.png" : "Images/Start.png";

        public bool IsTicketDefault => this.TicketNumber.Equals("Ticket");

        public string TicketTextColour => IsTicketDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsGroupDefault => this.Group.Equals("Group");

        public string GroupTextColour => IsGroupDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsJobDefault => this.JobNumber.Equals("Job");

        public string JobTextColour => IsJobDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsDescriptionDefault => this.Description.Equals(DefaultDescription);

        public string DescriptionTextColour => IsDescriptionDefault ? "#aeaeae" : "#4e4e4e";

        public string StartStopTooltip => IsTimerRunning ? "Stop" : "Start";

        public void Update(IEnumerable<TimeEntry> entries)
        {
            foreach (var item in entries.Select(e => e.Group).Distinct())
            {
                this.GroupItems.Add(item);
            }

            var currentJobs = entries.Where(e => !string.IsNullOrWhiteSpace(e.Job)).Select(e => e.Job).ToHashSet();
            var currentTickets = entries.Where(e => !string.IsNullOrWhiteSpace(e.Ticket)).Select(e => e.Ticket).ToHashSet();
            var currentGroups = entries.Where(e => !string.IsNullOrWhiteSpace(e.Group)).Select(e => e.Group).ToHashSet();

            // Remove old
            foreach (var job in PreviousJobs.ToList())
            {
                if (!currentJobs.Contains(job))
                {
                    PreviousJobs.Remove(job);
                }
            }

            // Add new
            foreach (var job in currentJobs.ToList())
            {
                if (!PreviousJobs.Contains(job))
                {
                    PreviousJobs.Add(job);
                }
            }


            // Remove old
            foreach (var job in PreviousGroups.ToList())
            {
                if (!currentGroups.Contains(job))
                {
                    PreviousGroups.Remove(job);
                }
            }

            // Add new
            foreach (var job in currentGroups.ToList())
            {
                if (!PreviousGroups.Contains(job))
                {
                    PreviousGroups.Add(job);
                }
            }

            // Remove old
            foreach (var ticket in PreviousTickets.ToList())
            {
                if (!currentTickets.Contains(ticket))
                {
                    PreviousTickets.Remove(ticket);
                }
            }

            // Add new
            foreach (var ticket in currentTickets.ToList())
            {
                if (!PreviousTickets.Contains(ticket))
                {
                    PreviousTickets.Add(ticket);
                }
            }

            previousUpdate = DateTime.Now.AddSeconds(1);
        }

        public bool StartNew()
        {
            if (IsInputValid())
            {
                this.UpdateStartTime();
                this.AddJobToHistory();
                this.PlayStartAnimation();
                this.StartTimer();
                this.StartHours = 0;
                this.StartMinutes = 0;
                
                if (!this.GroupItems.Contains(this.Group) && !string.IsNullOrEmpty(this.Group))
                {
                    this.GroupItems.Add(this.Group);
                }

                return true;
            }

            return false;
        }

        public void Stop()
        {
            if (this.IsTimerRunning)
            {
                this.IsTimerRunning = false;
                this.JobNumber = "Job";
                this.TicketNumber = "Ticket";
                this.Description = DefaultDescription;
                this.JobDuration = new TimeSpan();
                this.Group = string.Empty;
                this.PlayStopAnimation();

                var command = CommandHandler.GetCommand(EntryManager.GetCurrentEntryCommandId()) as StartEntryCommand;
                if (command != null)
                {
                    command.End = DateTime.Now;
                }
            }
        }

        private void AddJobToHistory()
        {
            var addEntryCommand = new StartEntryCommand(
                Guid.NewGuid(),
                this.StartTime.Ticks,
                this.JobNumber.Equals("Job") ? string.Empty : this.JobNumber,
                this.TicketNumber.Equals("Ticket") ? string.Empty : this.TicketNumber,
                this.Description.Equals(this.DefaultDescription) ? string.Empty : this.Description,
                this.Group,
                ColourPickerViewModel.SelectedColour);

            CommandHandler.Execute(addEntryCommand);
        }

        private void UpdateStartTime()
        {
            this.StartTime = DateTime.Now;
            this.StartTime = this.StartTime.AddHours(-this.JobDuration.Hours);
            this.StartTime = this.StartTime.AddMinutes(-this.JobDuration.Minutes);
            this.StartTime = this.StartTime.AddSeconds(-this.JobDuration.Seconds);
        }
        private void StartTimer()
        {
            this.IsTimerRunning = true;
        }

        private bool IsInputValid()
        {
            if (string.IsNullOrEmpty(this.JobNumber) && string.IsNullOrEmpty(this.TicketNumber) && string.IsNullOrEmpty(this.Description))
            {
                this.ToastManager.ShowToast(
                    "Start Failed",
                    "You must include a job number, ticket number or description.",
                    3,
                    false);

                return false;
            }

            if (this.JobNumber.Equals("Job") && this.TicketNumber.Equals("Ticket") && this.Description.Equals(DefaultDescription))
            { 
                this.ToastManager.ShowToast(
                    "Start Failed",
                    "You must set either a job number, ticket number or description.",
                    3,
                    false);

                return false;
            }

            if (IsTimerRunning)
            {
                this.ToastManager.ShowToast(
                    "Start Failed",
                    "You are already running a job.",
                    3,
                    false);

                return false;
            }

            return true;
        }

        public TabContentManagerViewModel TabContentManagerViewModel { get; set; }

        public bool IsTimerRunning { get; set; }
        
        public Timer Timer { get; set; }

        public DateTime StartTime { get; set; }

        public TimeSpan JobDuration { get; set; }
        
        public string DisplayDuration
        {
            get => JobDuration.ToString(@"hh\:mm\:ss");
            set
            {
                var hms = value.Split(':');
                if(hms.Count() == 3 && 
                    int.TryParse(hms[0], out var h) && 
                    int.TryParse(hms[1], out var m) && 
                    int.TryParse(hms[2], out var s))
                {
                    JobDuration = new TimeSpan(h, m, s);
                }
            }
        }

        public bool IsTimerNotRunning => !IsTimerRunning;

        public string JobNumber { get; set; } = "Job";

        public string TicketNumber { get; set; } = "Ticket";

        public string DefaultDescription => "What are you working on?";

        public string Description { get; set; } = "What are you working on?";

        public double StartHours { get; set; } = 0;

        public double StartMinutes { get; set; } = 0;

        public string Id => "TicketInput";

        public bool IsSelected { get; set; }

        private DateTime previousUpdate;

        private void UpdateTimer(object state)
        {
            if (IsTimerRunning)
            {
                JobDuration = DateTime.Now - StartTime;
                this.EntryManager.UpdateCurrentJob();
            }
            var currentTime = DateTime.Now;
            if((currentTime - previousUpdate).TotalSeconds > 1)
            {
                previousUpdate = currentTime;

                var newEvent = EventManager.CheckEvents(IsTimerRunning 
                    ? this.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.First()
                    : null);

                if (newEvent != null)
                {
                    Stop();
                    StartNewJob(newEvent);
                }
            }
        }

        private void StartNewJob(Event e)
        {
            JobNumber = e.Job;
            TicketNumber = e.Ticket;
            Description = e.Description;
            StartHours = 0;
            StartMinutes = 0;

            this.StartNew();
        }

        public UserControl GetContent()
        {
            return content;
        }
    }
}