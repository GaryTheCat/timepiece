﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;

namespace Timepiece_V2.UIControls.ViewConductor
{
    /// <summary>
    /// Interaction logic for ConductorView.xaml
    /// </summary>
    public partial class ConductorView : UserControl
    {
        public ConductorView()
        {
            InitializeComponent();

            MessageBus.Listen<LeftPaneGotFocusEvent>(LeftPaneGotFocus);
        }

        private void LeftPaneGotFocus()
        {
            LeftPaneBorder.StartAnimation(
                Color.FromRgb(0x3D, 0x8E, 0xF5),
                Color.FromRgb(0xF5, 0xf5, 0xf5),
                0.6,
                true);
        }
    }
}
