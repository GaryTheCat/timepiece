﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.Enums;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.Interfaces;
using Timepiece_V2.Services;

namespace Timepiece_V2.UIControls.ViewConductor
{
    public class ConductorViewModel : ReactiveViewModelBase
    {
        public ConductorViewModel()
        {
        }

        private Dictionary<string, IControl> Items { get; set; }
            = new Dictionary<string, IControl>();

        private IControl SelectedControl { get; set; }

        public UserControl Content => SelectedControl.GetContent();

        public Border Border { get; internal set; }

        public void SetInitialContent(string id)
        {
            SelectedControl = Items[id];
        }

        public void SetContent(string id, AnimationType type)
        {
            if (!SelectedControl.Id.Equals(id))
            {
                if (Items.TryGetValue(id, out var control))
                {
                    var newControl = control;
                    if (type.Equals(AnimationType.FadeOutIn))
                    {
                        Content.StartAnimation(1, 0, 0.15, UIElement.OpacityProperty, () =>
                        {
                            SelectedControl = newControl;
                            Content.StartAnimation(0, 1, 0.15, UIElement.OpacityProperty, () => { });
                        });
                    }
                }
            }
        }

        public void AddItem(string id, IControl control)
        {
            Items.Add(id, control);
        }

        public T GetControlModel<T>(string id = "") where T : IControl
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Items.Values.OfType<T>().First();
            }
            else
            {
                return Items.Values.OfType<T>().First(i => i.Id.Equals(id));
            }
        }
    }
}
