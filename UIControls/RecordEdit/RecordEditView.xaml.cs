﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Timepiece_V2.Enums;
using Timepiece_V2.Helpers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.ThisSprint;

namespace Timepiece_V2.UIControls.RecordEdit
{
    /// <summary>
    /// Interaction logic for RecordEditView.xaml
    /// </summary>
    public partial class RecordEditView : UserControl
    {
        public RecordEditViewModel ViewModel => this.DataContext as RecordEditViewModel;

        public RecordEditView()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var startTime = DateTimeHelper.GetTime(ViewModel.FromTime);

            if (startTime == null)
            {
                ViewModel.ToastManager.ShowToast(
                    "Start Time Invalid",
                    "Your start time should take the form hh:mm.",
                    durationSeconds: 3,
                    successful: false);

                return;
            }

            var endTime = DateTimeHelper.GetTime(ViewModel.ToTime);

            if (endTime == null)
            {
                ViewModel.ToastManager.ShowToast(
                    "End Time Invalid",
                    "Your end time should take the form hh:mm.",
                    durationSeconds: 3,
                    successful: false);
                return;
            }

            var duration = DateTimeHelper.GetDuration(ViewModel.Duration);

            if (duration == null)
            {
                ViewModel.ToastManager.ShowToast(
                    "Duration Invalid",
                    "Your duration time should take the form 00h 00m.",
                    durationSeconds: 3,
                    successful: false);
                return;
            }
            
            ViewModel.TimeEntry.Job = ViewModel.IsJobDefault ? string.Empty : ViewModel.Job;
            ViewModel.TimeEntry.Ticket = ViewModel.IsTicketDefault ? string.Empty : ViewModel.Ticket;
            ViewModel.TimeEntry.Group = ViewModel.IsGroupDefault ? string.Empty : ViewModel.Group;
            ViewModel.TimeEntry.Description = ViewModel.IsDescriptionDefault ? string.Empty : ViewModel.Description;
            ViewModel.TimeEntry.Start = DateTimeHelper.GetDateTime(ViewModel.TimeEntry.Start, ViewModel.Date, ViewModel.FromTime);
            ViewModel.TimeEntry.End = DateTimeHelper.GetDateTime(ViewModel.TimeEntry.End, ViewModel.Date, ViewModel.ToTime);
            ViewModel.TimeEntry.Colour = ViewModel.ColourPickerViewModel.SelectedColour;

            ViewModel.Conductor.SetContent("TicketInput", AnimationType.FadeOutIn);

            MessageBus.Send<EntryTimeUpdatedEvent>();
            MessageBus.Send<EntryUpdatedEvent>();
        }

        private void Cancel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Conductor.SetContent("TicketInput", AnimationType.FadeOutIn);
        }

        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = ((Button)sender).Parent as Border;
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xe4, 0xe4, 0xe4));
        }

        private void Button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = ((Button)sender).Parent as Border;
            border.BorderBrush = new SolidColorBrush(Colors.White);
        }

        private void From_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            FromTextBox.Focus();
            FromTextBox.SelectionStart = FromTextBox.Text.Length;
            FromTextBox.SelectionLength = 0;
        }

        private void To_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ToTextBox.Focus();
            ToTextBox.SelectionStart = ToTextBox.Text.Length;
            ToTextBox.SelectionLength = 0;
        }

        private void Duration_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DurationTextBox.Focus();
            DurationTextBox.SelectionStart = DurationTextBox.Text.Length;
            DurationTextBox.SelectionLength = 0;
        }

        private void JobComboBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Job = ViewModel.IsJobDefault ? string.Empty : ViewModel.Job;
        }

        private void JobComboBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Job = string.IsNullOrWhiteSpace(ViewModel.Job) ? "Job" : ViewModel.Job;
        }

        private void TicketComboBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Ticket = ViewModel.IsTicketDefault ? string.Empty : ViewModel.Ticket;
        }

        private void TicketComboBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Ticket = string.IsNullOrWhiteSpace(ViewModel.Ticket) ? "Ticket" : ViewModel.Ticket;
        }

        private void GroupComboBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Group = ViewModel.IsGroupDefault ? string.Empty : ViewModel.Group;
        }

        private void GroupComboBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Group = string.IsNullOrWhiteSpace(ViewModel.Group) ? "Group" : ViewModel.Group;
        }

        private void Description_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Description = ViewModel.IsDescriptionDefault ? string.Empty : ViewModel.Description;
        }

        private void Description_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            ViewModel.Description = string.IsNullOrWhiteSpace(ViewModel.Description) ? ViewModel.DefaultDescription : ViewModel.Description;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter) || Keyboard.IsKeyDown(Key.Return))
            {
                Save_Click(null, null);
            }
        }
    }
}
