﻿using System;
using System.Windows.Controls;
using Timepiece_V2.Helpers;
using Timepiece_V2.Interfaces;
using Timepiece_V2.Managers;
using Timepiece_V2.UIControls.ColourPicker;
using Timepiece_V2.UIControls.ViewConductor;

namespace Timepiece_V2.UIControls.RecordEdit
{
    public class RecordEditViewModel : ReactiveViewModelBase, IControl
    {
        public RecordEditViewModel(ConductorViewModel conductor, ToastManager toastManager)
        {
            this.ToastManager = toastManager;

            recordEditView = new RecordEditView();

            recordEditView.DataContext = this;

            ColourPickerViewModel = new ColourPickerViewModel();

            this.Conductor = conductor;
        }

        private TimeEntry timeEntry;

        internal string DefaultDescription = "What were you working on?";

        public TimeEntry TimeEntry {
            get => timeEntry;
            set
            {
                Job = string.IsNullOrEmpty(value.Job) ? "Job" : value.Job;
                Ticket = string.IsNullOrEmpty(value.Ticket) ? "Ticket" : value.Ticket;
                Group = string.IsNullOrEmpty(value.Group) ? "Group" : value.Group;
                Description = string.IsNullOrEmpty(value.Description) ? DefaultDescription : value.Description;
                fromTime = $"{value.Start.Hour.ToString("00")}:{value.Start.Minute.ToString("00")}";
                toTime = $"{value.End.Hour.ToString("00")}:{value.End.Minute.ToString("00")}";
                var durations = (value.End - value.Start);
                duration = $"{durations.Hours.ToString("00")}h {durations.Minutes.ToString("00")}m";
                timeEntry = value;
                ColourPickerViewModel.SelectedColour = value.Colour;

                Date = value.Start;

                if(ColourPickerViewModel.View != null)
                {
                    ColourPickerViewModel.View.SetSelection(value.Colour);
                }
            }
        }

        public DateTime Date { get; set; }

        public string Id => "EditRecord";

        private string selectedColour = "#3D8EF5";

        public ColourPickerViewModel ColourPickerViewModel { get; set; }

        public ToastManager ToastManager { get; set; }

        public bool IsSelected { get; set; }

        public ConductorViewModel Conductor { get; set; }

        private UserControl recordEditView { get; set; }

        public string Job { get; set; }

        public string Ticket { get; set; }

        public string Group { get; set; }

        public string Description { get; set; }

        private string fromTime { get; set; }

        public bool IsTicketDefault => this.Ticket.Equals("Ticket");

        public string TicketTextColour => IsTicketDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsJobDefault => this.Job.Equals("Job");

        public string JobTextColour => IsJobDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsDescriptionDefault => this.Description.Equals(DefaultDescription);

        public string DescriptionTextColour => IsDescriptionDefault ? "#aeaeae" : "#4e4e4e";

        public bool IsGroupDefault => this.Group.Equals("Group");

        public string GroupTextColour => IsGroupDefault ? "#aeaeae" : "#4e4e4e";

        public bool FromThickness { get; set; }

        public bool ToThickness { get; set; }

        public bool DurationThickness { get; set; }

        public string FromTime
        {
            get => fromTime;
            set
            {
                fromTime = value;

                var startTime = DateTimeHelper.GetTime(value);
                var endTime = DateTimeHelper.GetTime(toTime);

                if (startTime != null && endTime != null)
                {
                    var durations = (endTime - startTime).Value;
                    duration = $"{durations.Hours.ToString("00")}h {durations.Minutes.ToString("00")}m";
                }
            }
        }

        private string toTime { get; set; }

        public string ToTime
        {
            get => toTime;
            set
            {
                toTime = value;

                var startTime = DateTimeHelper.GetTime(fromTime);
                var endTime = DateTimeHelper.GetTime(value);
                if (endTime != null && startTime != null)
                {
                    var durations = (endTime - startTime).Value;
                    duration = $"{durations.Hours.ToString("00")}h {durations.Minutes.ToString("00")}m";
                }
            }
        }

        private string duration { get; set; }

        public string Duration
        {
            get => duration;
            set
            {
                duration = value;

                var tempDuration = DateTimeHelper.GetDuration(value);

                if (tempDuration == null)
                {
                    return;
                }

                var startTime = DateTimeHelper.GetTime(fromTime);

                var endTime = startTime + tempDuration;
                toTime = $"{endTime.Value.Hour.ToString("00")}:{endTime.Value.Minute.ToString("00")}";
            }
        }

        public UserControl GetContent()
        {
            return recordEditView;
        }
    }
}
