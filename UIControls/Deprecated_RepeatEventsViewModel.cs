﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Managers;
using Timepiece_V2.Notifications;

namespace Timepiece_V2.UIControls
{
    public class Deprecated_RepeatEventsViewModel : ReactiveViewModelBase
    {
        public string WindowDisplayName => $"Repeated Events {Configuration.DisplayVersion}";
        
        public MainWindowViewModel MainWindowViewModel { get; set; }

        public ToastNotificationViewModel ToastNotificationViewModel { get; set; }

        public ReocurringEventManager ReocurringEventManaer { get; set; }

        public ToastManager ToastManager { get; set; }

        public bool SelectAll { get; set; }

        public Deprecated_RepeatEventsViewModel(
            MainWindowViewModel mainWindowViewModel, 
            ReocurringEventManager manager,
            ToastManager toastManager)
        {
            this.ToastManager = toastManager;
            this.MainWindowViewModel = mainWindowViewModel;
            this.ReocurringEventManaer = manager;
            this.ToastNotificationViewModel = new ToastNotificationViewModel();
            this.ToastManager.AddToast(this.ToastNotificationViewModel, ToastLocation.ReoccurringWindow);

            for (int i = 0; i < 4; i++)
            {
                Reoccurances.Add((Reoccurance)i);
            }
            for (int i = 0; i < 7; i++)
            {
                DaysOfWeek.Add(new Day
                {
                    DayOfWeek = (DayOfWeek)i,
                    IsEnabled = !(i == 0 || i == 6)
                });
            }
        }

        public void SetAllEvents()
        {
            foreach (var e in Events)
            {
                e.IsChecked = SelectAll;
            }
        }

        public void DeleteSelectedEvents()
        {
            var eventsToDelete = this.Events.Where(e => e.IsChecked).ToList();

            if (eventsToDelete.Any())
            {
                eventsToDelete.ForEach(e => this.Events.Remove(e));
                this.ToastManager.ShowToast("Events Removed", $"{eventsToDelete.Count} Events have been succesfully removed", 3, true);
                SelectAll = false;
            }
            else
            {
                this.ToastManager.ShowToast("No Events Removed", "Check the events you want to delete", 3, false);
            }
        }

        public void AddEvent()
        {
            StartDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day);

            if (ValidateEvent())
            {
                Events.Add(new Event
                {
                    Id = Guid.NewGuid(),
                    Name = Name,
                    Ticket = Ticket,
                    Job = Job,
                    Description = Description,
                    Days = DaysOfWeek.Where(d => d.IsEnabled).Select(d => d.DayOfWeek).ToList(),
                    Duration = Duration,
                    Reoccurance = SelectedReoccurance,
                    Start = StartDate.AddHours(hours).AddMinutes(minutes),
                    NextOccurance = StartDate.AddHours(hours).AddMinutes(minutes)
                });
            }
        }

        private bool ValidateEvent()
        {
            if (Name.Equals("Event"))
            {
                ToastManager.ShowToast("Event Not Added", "You must provide a name for your event.", 3, false);
                return false;
            }
            if (!DaysOfWeek.Any(d => d.IsEnabled))
            {
                ToastManager.ShowToast("Event Not Added", "You must have at least 1 weekday selected.", 3, false);
                return false;
            }
            if(string.IsNullOrWhiteSpace(Job) && string.IsNullOrWhiteSpace(Ticket))
            {
                ToastManager.ShowToast("Event Not Added", "You must provide either a ticket or job.", 3, false);
                return false;
            }

            return true;
        }
        
        public ObservableCollection<Event> Events { get; set; }
            = new ObservableCollection<Event>();

        public List<Day> DaysOfWeek { get; set; }
            = new List<Day>();

        public DayOfWeek SelectedDayOfWeek { get; set; }

        public List<Reoccurance> Reoccurances { get; set; }
            = new List<Reoccurance>();

        private Reoccurance selectedReoccurance { get; set; }

        public Reoccurance SelectedReoccurance { get => selectedReoccurance;
            set
            {
                var clear = value != Reoccurance.Daily && selectedReoccurance == Reoccurance.Daily;

                selectedReoccurance = value;
                if(selectedReoccurance == Reoccurance.Daily)
                {
                    for(int i = 0; i < 7; i++)
                    {
                        DaysOfWeek[i].IsEnabled = !(i == 0 || i == 6);
                    }
                }

                if (clear)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        DaysOfWeek[i].IsEnabled = false;
                    }
                }
            }
        }

        private int hours = 9;

        private int minutes = 0;

        private string startTime = "09:00";
        
        public string StartTime
        {
            get => startTime;
            set
            {
                var values = value.Split(':');
                if (values.Length == 2 && int.TryParse(values[0], out var hour) && int.TryParse(values[1], out var minute))
                {
                    hours = hour;
                    minutes = minute;
                    startTime = $"{hours.ToString("00")}:{minutes.ToString("00")}";
                    OnPropertyChanged("StartTime");
                }
            }
        }

        public string Name { get; set; } = "Event";

        public string Ticket { get; set; }

        public string Job { get; set; }

        public string Description { get; set; }

        public double Duration { get; set; } = 30;

        public DateTime StartDate { get; set; } = DateTime.Now;

        public bool AreDaysEnabled => SelectedReoccurance != Reoccurance.Daily;

        public void Save()
        {
            // Create a stream to serialize the object to.  
            var ms = new MemoryStream();

            // Serializer the User object to the stream.  
            var ser = new DataContractJsonSerializer(typeof(Event));

            foreach (var e in Events)
            {
                ser.WriteObject(ms, e);
            }

            File.WriteAllBytes(Configuration.EventsJSON, ms.ToArray());
        }

        public void Load()
        {
            if (File.Exists(Configuration.EventsJSON))
            {
                var str = File.ReadAllText(Configuration.EventsJSON);
                var splitEntries = str.Split('{').Skip(1).Select(e => $"{{{e}");

                var deser = new DataContractJsonSerializer(typeof(Event));
                MemoryStream ms;
                foreach (var entry in splitEntries)
                {
                    byte[] byteArray = Encoding.ASCII.GetBytes(entry);
                    ms = new MemoryStream(byteArray);
                    Events.Add(deser.ReadObject(ms) as Event);
                }
            }
        }
    }
}
