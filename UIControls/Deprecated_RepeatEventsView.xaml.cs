﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.Helpers;

namespace Timepiece_V2.UIControls
{
    /// <summary>
    /// Interaction logic for RepeatEventsView.xaml
    /// </summary>
    public partial class Deprecated_RepeatEventsView : Window
    {
        private Deprecated_RepeatEventsViewModel ViewModel => (Deprecated_RepeatEventsViewModel)this.DataContext;

        public Deprecated_RepeatEventsView()
        {
            InitializeComponent();
        }

        private void AddEvent_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.AddEvent();
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            ViewModel.ReocurringEventManaer.UpdateEvents(ViewModel.Events.ToList());
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.DeleteSelectedEvents();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.SetAllEvents();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = ((TextBox)sender);
            if (textBox.Text.Equals("Event"))
            {
                textBox.Text = string.Empty;
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = ((TextBox)sender);
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = "Event";
            }
        }
    }
}
