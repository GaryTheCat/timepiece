﻿using System.Collections.Generic;

namespace Timepiece_V2.UIControls.ScrollingTimeSelector
{
    public class ScrollingTimeSelectorViewModel
    {

        public ScrollingTimeSelectorViewModel()
        {
            Minutes.Add("");
            for (int i = 0; i < 61; i++)
            {
                Minutes.Add(i.ToString("00"));
            }
            Minutes.Add("");
        }

        public List<string> Minutes { get; set; } = new List<string>();
    }
}
