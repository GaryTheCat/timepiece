﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Timepiece_V2.UIControls.ScrollingTimeSelector
{
    /// <summary>
    /// Interaction logic for ScrollingTimeSelector.xaml
    /// </summary>
    public partial class ScrollingTimeSelector : UserControl
    {
        public ScrollingTimeSelector()
        {
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            MinutesScrollViewer.ScrollToVerticalOffset(MinutesScrollViewer.VerticalOffset + 19.5);
        }
    }
}
