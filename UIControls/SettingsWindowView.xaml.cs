﻿using System.Windows;
using System.Windows.Controls;

namespace Timepiece_V2
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindowView : Window
    {
        SettingsWindowViewModel ViewModel => (SettingsWindowViewModel)this.DataContext;

        public SettingsWindowView()
        {
            InitializeComponent();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Text = string.Empty;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.MainWindowViewModel.OnPropertyChanged("Colour1");
            this.ViewModel.MainWindowViewModel.OnPropertyChanged("Colour2");
        }

        private void UseImage_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.MainWindowViewModel.TabContentManagerViewModel.OnPropertyChanged("UseImage");
        }

        private void Compile_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Compile(true);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ViewModel.Save();
            this.ViewModel.MainWindowViewModel.IsSettingsClosed = true;
            this.ViewModel.MainWindowViewModel.OnPropertyChanged("TotalTime");
        }
    }
}
