﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Helpers;
using Timepiece_V2.Managers;
using Timepiece_V2.Notifications;
using Timepiece_V2.UIControls.ThisSprint;

namespace Timepiece_V2
{
    public class SettingsWindowViewModel : ReactiveViewModelBase
    {
        public string WindowDisplayName => $"Settings {Configuration.DisplayVersion}";

        public MainWindowViewModel MainWindowViewModel { get; set; }

        public ToastNotificationViewModel ToastNotificationViewModel { get; set; }

        public ToastManager ToastManager { get; set; }

        public Settings Settings { get; set; }

        public SettingsWindowViewModel(MainWindowViewModel mainWindowViewModel, ToastManager toastManager)
        {
            this.Settings = new Settings(this);
            this.ToastManager = toastManager;
            this.ToastNotificationViewModel = new ToastNotificationViewModel();
            this.ToastManager.AddToast(this.ToastNotificationViewModel, ToastLocation.SettingsWindow);
            this.MainWindowViewModel = mainWindowViewModel;
        }
        
        public void Load()
        {
            if (File.Exists(Configuration.SettingsJSON))
            {
                var bytes = File.ReadAllBytes(Configuration.SettingsJSON);

                var deser = new DataContractJsonSerializer(typeof(Settings));
                
                MemoryStream ms = new MemoryStream(bytes);
                Settings = deser.ReadObject(ms) as Settings;
            }
        }

        public void Save()
        {            
            // Create a stream to serialize the object to.  
            var ms = new MemoryStream();

            // Serializer the User object to the stream.  
            var ser = new DataContractJsonSerializer(typeof(Settings));
            ser.WriteObject(ms, Settings);

            File.WriteAllBytes(Configuration.SettingsJSON, ms.ToArray());            
        }

        public string Compile(bool reportSuccess)
        {
            string exportCode;

            if (File.Exists(Configuration.ExportsFile))
            {
                exportCode = File.ReadAllText(Configuration.ExportsFile);
            }
            else
            {
                exportCode = File.ReadAllText(Directory.GetCurrentDirectory() + "\\" + "Export.cs");
            }

            var lines = exportCode.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();
            int startIndex = lines.IndexOf(lines.First(l => l.Contains("//// HEAD ////")))+1;

            exportCode = 
                CompileHelper.InsertJobData(
                    this.MainWindowViewModel.TabContentManagerViewModel.GetTab<ThisSprintViewModel>().AllTimeEntries.ToList(), 
                    this.MainWindowViewModel.SettingsWindowViewModel.Settings, 
                    exportCode);

            exportCode = CompileHelper.UpdateWithUserCode(exportCode, this.Settings.CustomExportScript);


            CSharpCodeProvider codeProvider = new CSharpCodeProvider();
            ICodeCompiler icc = codeProvider.CreateCompiler();
            CompilerParameters parameters = new CompilerParameters();
            parameters.ReferencedAssemblies.Add("System.Core.dll");
            var tempPath = Path.GetTempPath() + "\\" + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".exe";
            parameters.OutputAssembly = tempPath;
            parameters.GenerateExecutable = true;
            CompilerResults results = icc.CompileAssemblyFromSource(parameters, exportCode);

            if (results.Errors.HasErrors)
            {
                var error = results.Errors.Cast<CompilerError>().First();

                this.ToastManager.ShowToast(
                "Compile Failed",
                $"({error.Line - startIndex},{error.Column}) : {error.ErrorText}",
                5,
                false);

                return null;
            }
            else
            {
                if (reportSuccess)
                {
                    this.ToastManager.ShowToast(
                    "Successful",
                    "Your code has been compiled successfully.",
                    3,
                    true);
                }

                return tempPath;
            }
        }
    }
}