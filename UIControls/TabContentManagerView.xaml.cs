﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Timepiece_V2.DataStructures;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;

namespace Timepiece_V2
{
    /// <summary>
    /// Interaction logic for JobHistoryView.xaml
    /// </summary>
    public partial class TabContentManagerView : UserControl
    {
        public TabContentManagerViewModel ViewModel => (TabContentManagerViewModel)this.DataContext;
        
        public Dictionary<string, ITab> TabLookup => ViewModel.TabLookup;

        private const int FontSizeLarge = 17;
        
        private const int FontSizeSmall = 16;

        public TabContentManagerView()
        {
            InitializeComponent();

            MessageBus.Listen<RightPaneGotFocusEvent>(RightPaneGotFocus);
        }

        private void RightPaneGotFocus()
        {
            RightPaneBorder.StartAnimation(
                Color.FromRgb(0x3D, 0x8e, 0xf5),
                Color.FromRgb(0xF5, 0xf5, 0xf5),
                0.5,
                true);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TabLookup[ThisSprintText.Text].Header = ThisSprintText;
            TabLookup[ThisSprintText.Text].Accent = ThisSprintRectangle;

            TabLookup[ReocurringText.Text].Header = ReocurringText;
            TabLookup[ReocurringText.Text].Accent = ReocurringRectangle;

            TabLookup[AllTicketsText.Text].Header = AllTicketsText;
            TabLookup[AllTicketsText.Text].Accent = AllTicketsRectangle;
        }

        #region Mouse Enter/Leave
        private void AllTickets_MouseEnter(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                AllTicketsText.StartAnimation(
                    FontSizeSmall, 
                    FontSizeLarge, 
                    durationSeconds: 0.1, 
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }

        private void AllTickets_MouseLeave(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                AllTicketsText.StartAnimation(
                    FontSizeLarge,
                    FontSizeSmall,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }

        private void Reocurring_MouseEnter(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                ReocurringText.StartAnimation(
                    FontSizeSmall,
                    FontSizeLarge,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }

        private void Reocurring_MouseLeave(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                ReocurringText.StartAnimation(
                    FontSizeLarge,
                    FontSizeSmall,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }

        private void ThisSprint_MouseEnter(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                ThisSprintText.StartAnimation(
                    FontSizeSmall,
                    FontSizeLarge,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }

        private void ThisSprint_MouseLeave(object sender, MouseEventArgs e)
        {
            var header = ((TextBlock)sender).Text;
            if (!TabLookup[header].IsSelected)
            {
                ThisSprintText.StartAnimation(
                    FontSizeLarge,
                    FontSizeSmall,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });
            }
        }
        #endregion

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            // Get the selected tab
            var header = ((TextBlock)sender).Text;
            var tab = TabLookup[header];

            if (tab.IsSelected)
            {
                return;
            }

            tab.IsSelected = true;

            tab.Header.StartAnimation(
                    from: FontSizeSmall,
                    to: FontSizeLarge,
                    durationSeconds: 0.1,
                    TextBlock.FontSizeProperty,
                    () => { });

            FadeInElements(new List<FrameworkElement>
            {
                tab.GetContent(),
                tab.Accent
            });

            foreach (var otherTab in TabLookup.Values)
            {
                if(otherTab == tab)
                {
                    continue;
                }

                if (otherTab.IsSelected)
                {
                    otherTab.IsSelected = false;
                    otherTab.Header.StartAnimation(
                        from: FontSizeLarge,
                        to: FontSizeSmall,
                        durationSeconds: 0.1,
                        TextBlock.FontSizeProperty,
                        () => { });
                    FadeOutElements(new List<FrameworkElement>
                    {
                        otherTab.Accent,
                        otherTab.GetContent()
                    });
                }
            }

            ViewModel.SetSelectedTab(tab);
        }

        private void FadeOutElements(IEnumerable<FrameworkElement> elements)
        {
            foreach (var element in elements)
            {
                element.StartAnimation(
                    from: 1,
                    to: 0,
                    durationSeconds: 0.3,
                    OpacityProperty,
                    () => element.Visibility = Visibility.Collapsed);
            }
        }

        private void FadeInElements(IEnumerable<FrameworkElement> elements)
        {
            foreach (var element in elements)
            {
                element.Visibility = Visibility.Visible;
                element.StartAnimation(
                    from: 0,
                    to: 1,
                    durationSeconds: 0.3,
                    OpacityProperty,
                    () => { });
            }
        }

        private void Minimise_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowStateManager.MinimiseRightPane();
        }
    }
}