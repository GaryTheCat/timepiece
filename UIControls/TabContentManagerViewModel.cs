﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Managers;

namespace Timepiece_V2
{
    public class TabContentManagerViewModel : ReactiveViewModelBase
    {
        public TabContentManagerViewModel(WindowStateManager windowStateManager)
        {
            this.WindowStateManager = windowStateManager;
        }
                       
        public WindowStateManager WindowStateManager { get; set; }
        
        private List<ITab> ContentTabs { get; set; } = new List<ITab>();

        public Dictionary<string, ITab> TabLookup { get; set; } = new Dictionary<string, ITab>();

        public ITab SelectedTab { get; private set; }

        public UserControl SelectedContent => SelectedTab?.GetContent();

        public void AddTab(ITab tab)
        {
            ContentTabs.Add(tab);

            TabLookup.Add(tab.GetHeaderText(), tab);
        }

        public void SetSelectedTab(ITab tab)
        {
            SelectedTab = tab;
        }

        public T GetTab<T>(string tabId = "") where T : ITab 
        {
            if (!string.IsNullOrEmpty(tabId))
            {
                return ContentTabs.OfType<T>().First(t => t.Id.Equals(tabId));
            }
            else
            {
                return ContentTabs.OfType<T>().First();
            }
        }

        internal void Load()
        {

        }

        internal void Save()
        {

        }
    }
}