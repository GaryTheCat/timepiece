﻿using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.UIControls.ViewConductor;

namespace Timepiece_V2.UIControls.TimeEntryContextMenu
{
    public class TimeEntryContextMenuViewModel : ReactiveViewModelBase
    {
        public ConductorViewModel LeftHandPane { get; }

        public TimeEntryContextMenuViewModel(ConductorViewModel leftHandPane)
        {
            this.LeftHandPane = leftHandPane;
        }

        public UserControl View { get; set; }

        public Visibility Visible { get; set; } = Visibility.Collapsed;

        public int Left { get; set; }

        public int Right { get; set; }

        public int Bottom { get; set; }

        public int Top { get; set; }

        public double TopMargin => 3;

        public double MiddleMargin => 33;

        public double BottomMargin => 63;

        public double LeftMargin => 3;
        
        public double RightMargin => 63;

        public Button TopLeftButton { get; set; }

        public Button TopMiddleButton { get; set; }

        public Button TopRightButton { get; set; }

        public Button MiddleLeftButton { get; set; }

        public Button MiddleButton { get; set; }

        public Button MiddleRightButton { get; set; }

        public Button BottomLeftButton { get; set; }

        public Button BottomCentreButton { get; set; }

        public Button BottomRightButton { get; set; }

        public TimeEntry SelectedTimeEntry { get; set; }
    }
}
