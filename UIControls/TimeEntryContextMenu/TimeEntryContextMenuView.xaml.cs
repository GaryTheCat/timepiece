﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.Commands;
using Timepiece_V2.Enums;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.RecordEdit;

namespace Timepiece_V2.UIControls.TimeEntryContextMenu
{
    /// <summary>
    /// Interaction logic for TimeEntryContextMenuView.xaml
    /// </summary>
    public partial class TimeEntryContextMenuView : UserControl
    {
        private TimeEntryContextMenuViewModel ViewModel => this.DataContext as TimeEntryContextMenuViewModel;
        
        public TimeEntryContextMenuView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.View = this;
            ViewModel.BottomLeftButton = BottomLeftButton;
            ViewModel.BottomCentreButton = BottomMiddleButton;
            ViewModel.BottomRightButton = BottomRightButton;
            ViewModel.MiddleLeftButton = MiddleLeftButton;
            ViewModel.MiddleButton = MiddleButton;
            ViewModel.MiddleRightButton = MiddleRightButton;
            ViewModel.TopLeftButton = TopLeftButton;
            ViewModel.TopMiddleButton = TopMiddleButton;
            ViewModel.TopRightButton = TopRightButton;
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ViewModel.View.Visibility = Visibility.Collapsed;
        }

        private void StartAgain_Click(object sender, RoutedEventArgs e)
        {
            var ticketInput = WindowService.GetWindow<TicketInputViewModel>();

            var record = ViewModel.SelectedTimeEntry;
            ticketInput.JobNumber = record.Job;
            ticketInput.TicketNumber = record.Ticket;
            ticketInput.Description = record.Description;
            ticketInput.Group = record.Group;
            ticketInput.ColourPickerViewModel.SelectedColour = record.Colour;
            ticketInput.StartNew();

            ViewModel.View.Visibility = Visibility.Collapsed;
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            // Deselect any currently selected items
            var entryManager = WindowService.GetWindow<EntryManager>();
            entryManager.DeselectAll();

            var recordEdit = WindowService.GetWindow<RecordEditViewModel>();
            recordEdit.TimeEntry = ViewModel.SelectedTimeEntry;
            ViewModel.LeftHandPane.SetContent("EditRecord", AnimationType.FadeOutIn);
            MessageBus.Send<LeftPaneGotFocusEvent>();

            ViewModel.View.Visibility = Visibility.Collapsed;
        }

        private void MiddleRightButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            EditButton_Click(null, null);
        }

        private void SplitButton_Click(object sender, RoutedEventArgs e)
        {
            var commandHandler = WindowService.GetWindow<CommandHandler>();

            var entry = ViewModel.SelectedTimeEntry;

            var previousEnd = entry.End;

            entry.End = entry.Start.AddMilliseconds((entry.End - entry.Start).TotalMilliseconds / 2);

            var addEntryCommand = new AddEntryCommand(
                Guid.NewGuid(),
                entry.End.Ticks,
                previousEnd.Ticks,
                entry.Job,
                entry.Ticket, 
                entry.Description, 
                entry.Group, 
                entry.Colour);

            commandHandler.Execute(addEntryCommand);

            ViewModel.View.Visibility = Visibility.Collapsed;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            // Deselect any currently selected items
            var commandHandler = WindowService.GetWindow<CommandHandler>();

            var record = ViewModel.SelectedTimeEntry;

            var deleteCommand = new DeleteEntriesCommand(new List<TimeEntry> { record });

            commandHandler.Execute(deleteCommand);

            ViewModel.View.Visibility = Visibility.Collapsed;
        }
    }
}