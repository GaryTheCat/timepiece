﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.DataStructures;
using Timepiece_V2.ExtensionMethods;

namespace Timepiece_V2.UIControls.Recurring
{
    public class RecurringViewModel : ReactiveViewModelBase, ITab
    {
        public RecurringViewModel()
        {
            header = "Recurring";

            content = new RecurringContentView();

            content.Opacity = 0;
            content.DataContext = this;
        }

        public ObservableCollection<Event> Events { get; set; }
            = new ObservableCollection<Event>();

        private RecurringContentView content;

        private string header;

        public FrameworkElement Accent { get; set; }

        public FrameworkElement Header { get; set; }

        public string Id => "RecurringEvents";

        public bool IsSelected { get; set; }

        public UserControl GetContent()
        {
            return content;
        }

        public string GetHeaderText()
        {
            return header;
        }

        public void Save()
        {
            // Create a stream to serialize the object to.  
            var ms = new MemoryStream();

            // Serializer the User object to the stream.  
            var ser = new DataContractJsonSerializer(typeof(Event));

            foreach (var e in Events)
            {
                ser.WriteObject(ms, e);
            }

            File.WriteAllBytes(Configuration.EventsJSON, ms.ToArray());
        }

        public void Load()
        {
            if (File.Exists(Configuration.EventsJSON))
            {
                var str = File.ReadAllText(Configuration.EventsJSON);
                var splitEntries = str.Split('{').Skip(1).Select(e => $"{{{e}");

                var deser = new DataContractJsonSerializer(typeof(Event));
                MemoryStream ms;

                foreach (var entry in splitEntries.Select(e => deser.ReadObject(new MemoryStream(Encoding.ASCII.GetBytes(e))) as Event).DistinctBy(e => e.Id))
                {
                    Events.Add(entry);
                }
            }

            OnPropertyChanged("Events");
        }
    }
}
