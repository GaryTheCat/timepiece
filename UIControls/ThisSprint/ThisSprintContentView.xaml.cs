﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Enums;
using Timepiece_V2.ExtensionMethods;
using Timepiece_V2.Managers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.RecordEdit;
using Timepiece_V2.UIControls.TimeEntryContextMenu;

namespace Timepiece_V2.UIControls.ThisSprint
{
    /// <summary>
    /// Interaction logic for ThisSprintContentView.xaml
    /// </summary>
    public partial class ThisSprintContentView : UserControl
    {
        public ThisSprintViewModel ViewModel => this.DataContext as ThisSprintViewModel;

        private Dictionary<Guid, Border> TimeEntryVisuals { get; set; } = new Dictionary<Guid, Border>();

        public ThisSprintContentView()
        {
            InitializeComponent();

            MessageBus.Listen<EntrySelectionChangedEvent>(StartSelectionAnimations);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.JobScrollViewer = JobScrollViewer;
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var timeEntryContextMenu = WindowService.GetWindow<TimeEntryContextMenuViewModel>();
            var position = LogicalPosition(button);
            timeEntryContextMenu.View.Visibility = Visibility.Visible;
            timeEntryContextMenu.Left = (int)(position.X - timeEntryContextMenu.View.ActualWidth / 2 + button.ActualWidth / 2);
            timeEntryContextMenu.Top = (int)(10 + timeEntryContextMenu.View.ActualHeight + position.Y - timeEntryContextMenu.View.ActualHeight / 2 + button.ActualWidth / 2);
            timeEntryContextMenu.SelectedTimeEntry = button.DataContext as TimeEntry;
            StartAnimation(timeEntryContextMenu);
        }

        private void StartAnimation(TimeEntryContextMenuViewModel timeEntryContextMenu)
        {
            timeEntryContextMenu.View.StartAnimation(0, 1, 0.1, OpacityProperty, () => { });
            var animationDuration = 0.1;

            // Top Left
            timeEntryContextMenu.TopLeftButton.StartAnimation(33, 3, animationDuration, OpacityProperty, () => { }, true);
            timeEntryContextMenu.TopLeftButton.StartAnimation(33, 3, animationDuration, Canvas.TopProperty, () => { }, true);
            timeEntryContextMenu.TopLeftButton.StartAnimation(33, 3, animationDuration, Canvas.LeftProperty, () => { }, true);

            // Top Middle
            timeEntryContextMenu.TopMiddleButton.StartAnimation(33, 3, animationDuration + 0.02, OpacityProperty, () => { }, true);
            timeEntryContextMenu.TopMiddleButton.StartAnimation(33, 3, animationDuration + 0.02, Canvas.TopProperty, () => { }, true);

            // Top Right
            timeEntryContextMenu.TopRightButton.StartAnimation(33, 3, animationDuration + 0.04, OpacityProperty, () => { }, true);
            timeEntryContextMenu.TopRightButton.StartAnimation(33, 3, animationDuration + 0.04, Canvas.TopProperty, () => { }, true);
            timeEntryContextMenu.TopRightButton.StartAnimation(33, 63, animationDuration + 0.04, Canvas.LeftProperty, () => { }, true);


            // Middle Left
            timeEntryContextMenu.MiddleLeftButton.StartAnimation(33, 3, animationDuration + 0.06, OpacityProperty, () => { }, true);
            timeEntryContextMenu.MiddleLeftButton.StartAnimation(33, 3, animationDuration + 0.06, Canvas.LeftProperty, () => { }, true);

            // Middle Right
            timeEntryContextMenu.MiddleRightButton.StartAnimation(33, 3, animationDuration + 0.08, OpacityProperty, () => { }, true);
            timeEntryContextMenu.MiddleRightButton.StartAnimation(33, 63, animationDuration + 0.08, Canvas.LeftProperty, () => { }, true);

            // Bottom Left
            timeEntryContextMenu.BottomLeftButton.StartAnimation(33, 3, animationDuration + 0.1, OpacityProperty, () => { }, true);
            timeEntryContextMenu.BottomLeftButton.StartAnimation(33, 3, animationDuration + 0.1, Canvas.LeftProperty, () => { }, true);
            timeEntryContextMenu.BottomLeftButton.StartAnimation(33, 63, animationDuration + 0.1, Canvas.TopProperty, () => { }, true);

            // Bottom Centre
            timeEntryContextMenu.BottomCentreButton.StartAnimation(33, 3, animationDuration + 0.12, OpacityProperty, () => { }, true);
            timeEntryContextMenu.BottomCentreButton.StartAnimation(33, 63, animationDuration + 0.12, Canvas.TopProperty, () => { }, true);

            // Bottom Right
            timeEntryContextMenu.BottomRightButton.StartAnimation(33, 3, animationDuration + 0.14, OpacityProperty, () => { }, true);
            timeEntryContextMenu.BottomRightButton.StartAnimation(33, 63, animationDuration + 0.14, Canvas.LeftProperty, () => { }, true);
            timeEntryContextMenu.BottomRightButton.StartAnimation(33, 63, animationDuration + 0.14, Canvas.TopProperty, () => { }, true);
        }

        private Point LogicalPosition(Control control)
        {
            UIElement container = VisualTreeHelper.GetParent(MainGrid) as UIElement;
            return control.TranslatePoint(new Point(0, 0), container);
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var senderGrid = sender as Grid;
            var timeEntry = senderGrid.DataContext as TimeEntry;
            timeEntry.IsSelected = !timeEntry.IsSelected;
             
            var border = senderGrid.Parent as Border;
            border.StartAnimation(
                timeEntry.IsSelected ? Color.FromRgb(0xFF, 0xFF, 0xFF) : Color.FromRgb(0x3D, 0x8E, 0xF5),
                timeEntry.IsSelected ? Color.FromRgb(0x3D, 0x8E, 0xF5) : Color.FromRgb(0xFF, 0xFF, 0xFF),
                0.15);
        }

        private void StartSelectionAnimations()
        {
            foreach(var entry in ViewModel.AllTimeEntries)
            {
                var timeEntryVisual = TimeEntryVisuals[entry.Id];
                if(entry.IsSelected && ((SolidColorBrush)timeEntryVisual.Background).Color.Equals(Colors.White))
                {
                    timeEntryVisual.StartAnimation(
                       Color.FromRgb(0xFF, 0xFF, 0xFF),
                       Color.FromRgb(0x3D, 0x8E, 0xF5),
                       0.15);
                }
                else if (!entry.IsSelected && !((SolidColorBrush)timeEntryVisual.Background).Color.Equals(Colors.White))
                {
                    timeEntryVisual.StartAnimation(
                       Color.FromRgb(0x3D, 0x8E, 0xF5),
                       Color.FromRgb(0xFF, 0xFF, 0xFF),
                       0.15);
                }
            }
        }

        private void Item_Loaded(object sender, RoutedEventArgs e)
        {
            var senderBorder = sender as Border;
            if(senderBorder.Background == null)
            {
                senderBorder.Background = new SolidColorBrush(Colors.White);
            }

            var timeEntry = senderBorder.DataContext as TimeEntry;
            TimeEntryVisuals[timeEntry.Id] = senderBorder;

            if (!TimeEntryVisuals.ContainsKey(timeEntry.Id))
            {
                TimeEntryVisuals.Add(timeEntry.Id, senderBorder);
            }
            else if (TimeEntryVisuals[timeEntry.Id] != senderBorder)
            {
                TimeEntryVisuals[timeEntry.Id] = senderBorder;
            }
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var dayRecords = (sender as TextBlock).DataContext as DayRecords;

            var selected = dayRecords.TimeEntries.Any(entry => !entry.IsSelected);
            foreach(var timeEntry in dayRecords.TimeEntries)
            {
                timeEntry.IsSelected = selected;
            }

            StartSelectionAnimations();
        }

        private void LnSizeNorth_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Start_Click(sender, null);
        }

        private void Textbox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            var recordEdit = WindowService.GetWindow<RecordEditViewModel>();
            recordEdit.TimeEntry = ((TextBlock)sender).DataContext as TimeEntry;

            TriggerEdit();
        }

        private void Ellipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            var recordEdit = WindowService.GetWindow<RecordEditViewModel>();
            recordEdit.TimeEntry = ((Ellipse)sender).DataContext as TimeEntry;

            TriggerEdit();
        }

        private void TriggerEdit()
        {
            // Deselect any currently selected items
            var entryManager = WindowService.GetWindow<EntryManager>();
            entryManager.DeselectAll();

            ViewModel.MainWindowViewModel.LeftHandPane.SetContent("EditRecord", AnimationType.FadeOutIn);
            MessageBus.Send<LeftPaneGotFocusEvent>();
        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Hand;
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }
    }
}