﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Windows;
using System.Windows.Controls;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Managers;
using Timepiece_V2.Notifications;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;
using Timepiece_V2.UIControls.ViewConductor;

namespace Timepiece_V2.UIControls.ThisSprint
{
    public class ThisSprintViewModel : ReactiveViewModelBase, ITab
    {
        public ThisSprintViewModel(EntryManager entryManager, ConductorViewModel leftPaneConductor)
        {
            header = "This Sprint";

            content = new ThisSprintContentView();

            content.DataContext = this;

            this.EntryManager = entryManager;

            this.LeftPaneconductor = leftPaneConductor;

            MessageBus.Listen<EntryUpdatedEvent>(SyncEntries);

            MessageBus.Listen<EntryTimeUpdatedEvent>(() => 
            {
                foreach(var day in Days)
                {
                    day.OnPropertyChanged(nameof(day.TotalTime));
                    day.OnPropertyChanged(nameof(day.DisplayName));
                }
            });
        }

        public EntryManager EntryManager { get; set; }

        public ConductorViewModel LeftPaneconductor { get; }

        public MainWindowViewModel MainWindowViewModel { get; set; }

        public ToastNotificationViewModel ToastNotificationViewModel { get; set; }

        public IEnumerable<TimeEntry> AllTimeEntries => Days.SelectMany(e => e.TimeEntries);

        public ObservableCollection<DayRecords> Days { get; set; }
            = new ObservableCollection<DayRecords>();

        public ScrollViewer JobScrollViewer { get; internal set; }

        public FrameworkElement Accent { get; set; }

        public FrameworkElement Header { get; set; }

        private void SyncEntries()
        {
            var entries = EntryManager.GetTimeEntries()
                .GroupBy(e => GetEntryKey(new DateTime(e.StartTicks)))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.ToList());

            // Remove old entries
            foreach(var day in this.Days)
            {
                if (!entries.TryGetValue(day.Id, out var timeEntries))
                {
                    day.TimeEntries.Clear();
                    continue;
                }

                foreach (var entry in day.TimeEntries.ToList())
                {
                    if (!timeEntries.Contains(entry))
                    {
                        day.TimeEntries.Remove(entry);
                    }
                }

                day.OnPropertyChanged(nameof(day.DisplayName));
            }
            
            // Add new entries
            foreach(var entry in entries)
            {
                if (!this.Days.Any(d => d.Id == entry.Key))
                {
                    var records = new DayRecords() { Id = entry.Key };
                    this.Days.Insert(GetDayIndex(records), records);
                }

                var day = this.Days.First(d => d.Id == entry.Key);

                foreach(var timeEntry in entry.Value)
                {
                    if (!day.TimeEntries.Contains(timeEntry))
                    {
                        day.TimeEntries.Insert(GetEntryIndex(day, timeEntry), timeEntry);
                    }
                }
            }

            // Remove days that no longer have entries
            foreach (var day in this.Days.ToList())
            {
                if (!day.TimeEntries.Any())
                {
                    this.Days.Remove(day);
                }
            }
         }

        public bool? SelectAllChecked { get; set; } = false;

        public bool IsSelected { get; set; }

        public string Id => "ThisSprint";

        private int GetDayIndex(DayRecords day)
        {
            var index = 0;
            foreach (var entry in this.Days)
            {
                if (day.Id < entry.Id)
                {
                    index = this.Days.IndexOf(entry) + 1;
                }
            }

            return index;
        }

        private int GetEntryIndex(DayRecords day, TimeEntry timeEntry)
        {
            var index = 0;
            foreach (var entry in day.TimeEntries)
            {
                if (timeEntry.Start < entry.Start)
                {
                    index = day.TimeEntries.IndexOf(entry) + 1;
                }
            }

            return index;
        }

        public DateTime GetEntryKey(DateTime startTime)
        {
            return new DateTime(startTime.Year, startTime.Month, startTime.Day);
        }
        
        public int RemoveSelectedJobs()
        {
            int i = 0;
            var daysToRemove = new List<DayRecords>();

            foreach (var day in this.Days)
            {
                foreach (var jobEntry in day.TimeEntries.Where(m => m.IsSelected).ToList())
                {
                    day.TimeEntries.Remove(jobEntry);

                    day.OnPropertyChanged("TotalTime");

                    if (!day.TimeEntries.Any())
                    {
                        daysToRemove.Add(day);
                    }
                    i++;
                }
            }

            foreach (var day in daysToRemove)
            {
                this.Days.Remove(day);
            }

            this.SelectAllChecked = false;
            return i;
        }

        private string header;

        private UserControl content;
        
        public UserControl GetContent()
        {
            return content;
        }

        public string GetHeaderText()
        {
            return header;
        }
    }
}
