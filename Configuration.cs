﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timepiece_V2
{
    public static class Configuration
    {
        private static bool IsRelease => true;

        public static string Version => "(V1.0.0)";

        public static string DisplayVersion => IsRelease ? string.Empty :  $"{Version}";

        public static string RootDirectory => $"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\\Timepiece";

        public static string BackupRecordsDirectory => $"{RootDirectory}\\PreviousSprints";

        public static string RecordsJSON => $"{RootDirectory}\\Records.json";

        public static string SettingsJSON => $"{RootDirectory}\\Settings.json";

        public static string EventsJSON => $"{RootDirectory}\\Events.json";

        public static string ExportsFile => $"{RootDirectory}\\Export.cs";

        public static string CommandsFile => $"{RootDirectory}\\Commands.json";

        public static string TempFile => $"{RootDirectory}\\lock";

        public static string GetBackupRecordsFileLocation(string fileName)
        {
            return $"{RootDirectory}\\PreviousSprints\\{fileName}.json";
        }
    }
}
