﻿using System.Collections.Generic;
using Timepiece_V2.Notifications;

namespace Timepiece_V2.Managers
{
    public enum ToastLocation
    {
        MainWindow,
        SettingsWindow,
        ReoccurringWindow,
        All
    }

    public class ToastManager
    {
        private List<ToastNotificationViewModel> toastViewModels;

        private Dictionary<ToastLocation, ToastNotificationViewModel> toastLocationLookup;

        public ToastManager()
        {
            toastViewModels = new List<ToastNotificationViewModel>();
            toastLocationLookup = new Dictionary<ToastLocation, ToastNotificationViewModel>();
        }

        public void AddToast(ToastNotificationViewModel toastViewModel, ToastLocation location)
        {
            toastViewModels.Add(toastViewModel);
            toastLocationLookup.Add(location, toastViewModel);
        }

        public void ShowToast(
            string title, 
            string description, 
            int durationSeconds,
            bool successful, 
            ToastLocation location = ToastLocation.All)
        {
            if(toastLocationLookup.TryGetValue(location, out var toastViewModel))
            {
                // Display the toast on a single screen
                toastViewModel.ShowIconToast(title, description, durationSeconds, successful);
            }
            else
            {
                // Display the toast on all screens
                foreach (var toast in toastViewModels)
                {
                    toast.ShowIconToast(title, description, durationSeconds, successful);
                }
            }  
        }
    }
}
