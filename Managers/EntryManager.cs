﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Helpers;
using Timepiece_V2.Services;
using Timepiece_V2.Services.Messages;

namespace Timepiece_V2.Managers
{
    /// <summary>
    /// Contains all logic for updating/removing/modifying time entries.
    /// After each update, views using the data should be notified.
    /// </summary>
    public class EntryManager
    {
        public EntryManager()
        {
            this.timeEntries = new List<TimeEntry>();
        }

        public Settings Settings { get; set; }

        public IEnumerable<string> PreviousJobs => timeEntries.Select(e => e.Job).Distinct();

        public IEnumerable<string> PreviousTickets => timeEntries.Select(e => e.Job).Distinct();

        private TimeEntry currentEntry;

        private List<TimeEntry> timeEntries;

        private Guid currentEntryCommandId;

        public void AddTimeEntries(List<TimeEntry> entries)
        {
            this.timeEntries.AddRange(entries);
            
            MessageBus.Send<EntryUpdatedEvent>();
        }

        public void StartNewTimeEntry(
            Guid commandId,
            Guid entryId,
            DateTime start,
            DateTime end,
            string job, 
            string ticket, 
            string description, 
            string group, 
            string colour)
        {
            currentEntry = new TimeEntry(entryId, start.Ticks, end.Ticks, job, ticket, description, group, colour);

            this.timeEntries.Add(currentEntry);

            this.currentEntryCommandId = commandId;

            MessageBus.Send<EntryUpdatedEvent>();
        }

        public void AddNewTimeEntry(
            Guid entryId,
            DateTime start,
            DateTime end,
            string job,
            string ticket,
            string description,
            string group,
            string colour)
        {
            var entry = new TimeEntry(entryId, start.Ticks, end.Ticks, job, ticket, description, group, colour);

            this.timeEntries.Add(entry);

            MessageBus.Send<EntryUpdatedEvent>();
        }

        public void DeleteTimeEntry(Guid entryId)
        {
            var entry = this.timeEntries.FirstOrDefault(e => e.Id.Equals(entryId));

            if (entry == null)
            {
                return;
            }

            if (entry == currentEntry)
            {
                MessageBus.Send<RunningEntryRemovedEvent>();
            }

            this.timeEntries.Remove(entry);

            MessageBus.Send<EntryUpdatedEvent>();
        }

        public int DeleteSelectedTimeEntries(List<TimeEntry> timeEntriesToDelete)
        {
            var groupedEntries = timeEntriesToDelete.GroupBy(e => SprintHelper.GetSprintName(Settings.SprintStartDate, e.Start));

            foreach(var sprint in groupedEntries)
            {
                SprintHelper.WriteSprint(sprint);
            }

            foreach(var entry in timeEntriesToDelete)
            {
                this.timeEntries.Remove(entry);
            }

            RecordHelper.RemoveRecords(Configuration.RecordsJSON, timeEntriesToDelete);

            MessageBus.Send<EntryUpdatedEvent>();

            return timeEntriesToDelete.Count();
        }

        public TimeEntry GetCurrentEntry()
        {
            return this.currentEntry;
        }

        public List<TimeEntry> GetTimeEntries()
        {
            return this.timeEntries;
        }

        public void DeselectAll()
        {
            foreach(var entry in timeEntries)
            {
                if (entry.IsSelected)
                {
                    entry.IsSelected = false;
                }
            }

            MessageBus.Send<EntrySelectionChangedEvent>();
        }

        public Guid GetCurrentEntryCommandId()
        {
            return this.currentEntryCommandId;
        }

        public void Load()
        {
            timeEntries = RecordHelper.LoadTimeEntries(Configuration.RecordsJSON);

            MessageBus.Send<EntryUpdatedEvent>();
        }

        public void UpdateCurrentJob()
        {
            currentEntry.End = DateTime.Now;

            currentEntry.OnPropertyChanged("End");

            MessageBus.Send<EntryTimeUpdatedEvent>();
        }

        public void Save()
        {
            RecordHelper.UpdateRecords(Configuration.RecordsJSON, this.timeEntries);
        }
    }
}
