﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timepiece_V2.DataStructures;
using Timepiece_V2.Helpers;

namespace Timepiece_V2.Managers
{
    public class ReocurringEventManager
    {
        private List<Event> reocurringEvents = new List<Event>();

        private Dictionary<Guid, Tuple<TimeEntry, Event>> insideEvents = 
            new Dictionary<Guid, Tuple<TimeEntry, Event>>();

        // Stack of events that have been paused due to reoccuring events.
        private List<TimeEntry> suspendedEntries = new List<TimeEntry>();
        
        // TODO: Fix this shit.
        // Takes in a list of events, and uses them to update the current event list.
        public void UpdateEvents(List<Event> events)
        {
            var incomingEvents = events.Select(e => e.Id).ToHashSet();
            var existingEvents = reocurringEvents.Select(e => e.Id).ToHashSet();
            
            // Remove old
            foreach(var e in reocurringEvents.ToList())
            {
                if (!incomingEvents.Contains(e.Id))
                {
                    reocurringEvents.Remove(e);
                }
            }

            // Add new
            foreach (var e in events)
            {
                if (!existingEvents.Contains(e.Id))
                {
                    reocurringEvents.Add(e);
                }
            }
        }

        public void AddEvent(Event e)
        {
            reocurringEvents.Add(e);
        }

        /// <summary>
        /// Check Events checks all reocurring events to determine if we are inside of one.
        /// </summary>
        /// <param name="currentlyRunning">The event that is currently running for 'Resume' functionality.</param>
        /// <returns>If we are inside an event, we return it, otherwise we return null.</returns>
        public Event CheckEvents(TimeEntry currentlyRunning)
        {
            Event insideEvent = null;
            foreach(var e in reocurringEvents)
            {
                if (EventHelper.AreInsideEvent(e) && !insideEvents.ContainsKey(e.Id))
                {
                    // We are starting a new event
                    insideEvents.Add(
                        e.Id,
                        new Tuple<TimeEntry, Event>(
                            currentlyRunning,
                            new Event()
                            {
                                Id = e.Id,
                                NextOccurance = e.NextOccurance,
                                Duration = e.Duration
                            }));
                    suspendedEntries.Add(currentlyRunning);
                    insideEvent = e;
                }
                else if(insideEvents.ContainsKey(e.Id) && !EventHelper.AreInsideEvent(insideEvents[e.Id].Item2))
                {
                    var previousEntry = insideEvents[e.Id].Item1;

                    // We have come out of a running event
                    insideEvent = new Event
                    {
                        Job = previousEntry.Job,
                        Ticket = previousEntry.Ticket,
                        Description = previousEntry.Description
                    };

                    insideEvents.Remove(e.Id);
                }

                // Update the next occurance to not trigger this event next update.
                if (e.NextOccurance < DateTime.Now)
                {
                    e.NextOccurance = EventHelper.GetFirstOccurance(e);
                }
            }

            return insideEvent;
        }
    }
}
