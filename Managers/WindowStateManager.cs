﻿using System;
using System.Windows;
using Timepiece_V2.Systems;

namespace Timepiece_V2.Managers
{
    public class WindowStateManager
    {
        public WindowStateManager(DataStructures.WindowState windowState, Action<string> onPropertyChanged)
        {
            this.windowState = windowState;
            this.onPropertyChanged = onPropertyChanged;
        }

        private DataStructures.WindowState windowState { get; set; }

        private Action<string> onPropertyChanged { get; set; }

        public void MinimiseLeftPane()
        {
            if (!windowState.IsRightPaneOpen)
            {
                MaximiseRightPane();
            }

            new DoubleAnimator(0.333, 0, 120, SetLeftPaneWidth, () => { });
            new DoubleAnimator(0.7, 0, 120, SetLeftOpenPaneOpacity, () => { });
            new DoubleAnimator(10, 20, 120, SetRightMarginThickness, () => windowState.IsLeftPaneOpen = false);

            windowState.OpenLeftPaneOpacity = 1;
            onPropertyChanged("WindowState");
        }

        public void MinimiseRightPane()
        {
            if (!windowState.IsLeftPaneOpen)
            {
                MaximiseLeftPane();
            }

            new DoubleAnimator(0.333, 0, 120, SetRightPaneWidth, () => { });
            new DoubleAnimator(0.7, 0, 120, SetRightOpenPaneOpacity, () => { });
            new DoubleAnimator(10, 20, 120, SetLeftMarginThickness, () => windowState.IsRightPaneOpen = false);

            windowState.OpenRightPaneOpacity = 1;
            onPropertyChanged("WindowState");
        }

        private void SetLeftPaneWidth(double width)
        {
            windowState.LeftPaneWitdhValue = width;
            onPropertyChanged("WindowState");
        }

        private void SetRightPaneWidth(double width)
        {
            windowState.RightPaneWitdhValue = width;
            onPropertyChanged("WindowState");
        }

        private void SetLeftOpenPaneOpacity(double opacity)
        {
            windowState.OpenLeftPaneOpacity = opacity;
            onPropertyChanged("WindowState");
        }

        private void SetRightOpenPaneOpacity(double opacity)
        {
            windowState.OpenRightPaneOpacity = opacity;
            onPropertyChanged("WindowState");
        }

        private void SetRightMarginThickness(double thickness)
        {
            windowState.RightPaneMargin = new Thickness(thickness, 20, 20, 20);
            onPropertyChanged("WindowState");
        }

        private void SetLeftMarginThickness(double thickness)
        {
            windowState.LeftPaneMargin = new Thickness(20, 20, thickness, 20);
            onPropertyChanged("WindowState");
        }

        public void MaximiseLeftPane()
        {
            new DoubleAnimator(0, 0.333, 120, SetLeftPaneWidth, () => { });
            new DoubleAnimator(0.7, 0, 120, SetLeftOpenPaneOpacity, () => { });
            new DoubleAnimator(20, 10, 120, SetRightMarginThickness, () => { });

            windowState.IsLeftPaneOpen = true;
            onPropertyChanged("WindowState");
        }

        public void MaximiseRightPane()
        {
            new DoubleAnimator(0, 1, 120, SetRightPaneWidth, () => { });
            new DoubleAnimator(0.7, 0, 120, SetRightOpenPaneOpacity, () => { });
            new DoubleAnimator(20, 10, 120, SetLeftMarginThickness, () => { });

            windowState.IsRightPaneOpen = true;
            onPropertyChanged("WindowState");
        }
    }
}
