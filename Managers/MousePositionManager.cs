﻿using System;
using System.Windows;

namespace Timepiece_V2.Managers
{
    public static class MousePositionManager
    {
        private static double windowWidth;

        private static double windowHeight;

        private static Point mousePosition;

        public static void UpdateMousePosition(double windowWidth, double windowHeight, Point mousePosition)
        {
            MousePositionManager.windowWidth = windowWidth;
            MousePositionManager.windowHeight = windowHeight;
            MousePositionManager.mousePosition = mousePosition;
        }

        public static bool IsInTop(double pixels)
        {
            return mousePosition.Y <= pixels;
        }

        public static bool IsInBottom(double pixels)
        {
            return (windowHeight - mousePosition.Y) <= pixels;
        }

        public static bool IsInLeft(double pixels)
        {
            return mousePosition.X <= pixels;
        }

        public static bool IsInRight(double pixels)
        {
            return (windowWidth - mousePosition.X) <= pixels;
        }

        public static bool IsInTopRight(double pixels)
        {
            return IsInTop(pixels) && IsInRight(pixels);
        }

        public static bool IsInBottomRight(double pixels)
        {
            return IsInBottom(pixels) && IsInRight(pixels);
        }

        public static bool IsInBottomLeft(double pixels)
        {
            return IsInBottom(pixels) && IsInLeft(pixels);
        }

        public static bool IsInTopLeft(double pixels)
        {
            return IsInTop(pixels) && IsInLeft(pixels);
        }

        public static double GetDistanceFromTopRight(double threshold)
        {
            if (!IsInTopRight(threshold))
            {
                return double.MaxValue;
            }
            else
            {
                return Math.Pow(mousePosition.X - windowWidth, 2) + Math.Pow(mousePosition.Y - 0, 2);
            }
        }

        public static double GetDistanceFromLeft(double threshold)
        {
            if (!IsInLeft(threshold))
            {
                return double.MaxValue;
            }
            else
            {
                return Math.Pow(mousePosition.X - 0, 2);
            }
        }

        public static double GetDistanceFromRight(double threshold)
        {
            if (!IsInRight(threshold))
            {
                return double.MaxValue;
            }
            else
            {
                return Math.Pow(windowWidth - mousePosition.X, 2);
            }
        }
    }
}
