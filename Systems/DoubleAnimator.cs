﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Timepiece_V2.Systems
{
    public class DoubleAnimator
    {
        private double stepSize = 0;
        private double currentValue = 0;
        private int step = 0;
        private int totalSteps = 0;
        private Timer animationTimer;
        private Action<double> callback;
        private Action completeCallback;
        private double to;

        public DoubleAnimator(
            double from, 
            double to, 
            int duration,
            Action<double> callback,
            Action completeCallback)
        {
            Task.Run(() =>
            {
                currentValue = from;
                this.callback = callback;
                this.completeCallback = completeCallback;
                this.to = to;
                totalSteps = (duration * 120 / 1000);
                this.stepSize = (to - from) / totalSteps;

                animationTimer = new Timer(
                    new TimerCallback(_ => UpdateCurrentValue()),
                    null,
                    duration,
                    duration / totalSteps);
            });
        }

        private void UpdateCurrentValue()
        {
            // Update current value
            currentValue += stepSize;
            step++;

            if (step == totalSteps)
            {
                callback(to);
                completeCallback();
                animationTimer.Dispose();
                return;
            }

            callback(currentValue);
        }
    }
}
