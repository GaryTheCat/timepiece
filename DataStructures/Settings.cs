﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace Timepiece_V2.DataStructures
{
    [DataContract]
    public class Settings : ReactiveViewModelBase
    {
        public Settings(SettingsWindowViewModel viewModel)
        {
            this.ViewModel = viewModel;
            GoalsExportLocation = Directory.GetCurrentDirectory();
        }

        public SettingsWindowViewModel ViewModel { get; set; }

        [DataMember]
        public string FirstName { get; set; } = "fName";

        [DataMember]
        public string LastName { get; set; } = "lName";

        [DataMember]
        public string JobPrefix { get; set; } = "JOB-";

        [DataMember]
        public string TicketPrefix { get; set; } = "EVO-";

        [DataMember]
        public string GoalsExportLocation { get; set; } = string.Empty;

        [DataMember]
        public bool UseImage { get; set; }

        [DataMember]
        public double HoursInSprint { get; set; } = 120;

        [DataMember]
        public bool UseCustomExport { get; set; } = false;

        [DataMember]
        public string CustomExportScript { get; set; }

        [DataMember]
        public DateTime SprintStartDate { get; set; }

        [DataMember]
        public double WindowWidth { get; set; }

        [DataMember]
        public double WindowHeight { get; set; }

        [DataMember]
        public bool IsMaximised { get; set; }
    }
}
