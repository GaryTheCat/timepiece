﻿namespace Timepiece_V2.DataStructures
{
    /// <summary>
    /// You shouldn't need a comment for this.
    /// </summary>
    public enum Reoccurance
    {
        Daily,
        Weekly,
        Fortnightly,
        Triweekly
    }
}