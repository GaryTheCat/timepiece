﻿using System.Runtime.Serialization;
using System.Windows;

namespace Timepiece_V2.DataStructures
{
    [DataContract]
    public class WindowState : ReactiveViewModelBase
    {
        [DataMember]
        public Thickness LeftPaneMargin { get; set; } = new Thickness(20, 20, 10, 20);

        [DataMember]
        public Thickness RightPaneMargin { get; set; } = new Thickness(10, 20, 20, 20);

        [DataMember]
        public double LeftPaneWitdhValue = 0.333;

        [DataMember]
        public bool IsLeftPaneOpen { get; set; } = true;

        public string LeftPaneWidth => $"{LeftPaneWitdhValue}*";

        [DataMember]
        public double RightPaneWitdhValue = 1;

        public string RightPaneWidth => $"{RightPaneWitdhValue}*";

        [DataMember]
        public bool IsRightPaneOpen { get; set; } = true;

        [DataMember]
        public double OpenLeftPaneOpacity { get; set; } = 0;

        public Visibility OpenLeftPaneVisibility => OpenLeftPaneOpacity == 0
            ? Visibility.Collapsed
            : Visibility.Visible;

        [DataMember]
        public double OpenRightPaneOpacity { get; set; } = 0;

        public Visibility OpenRightPaneVisibility => OpenRightPaneOpacity == 0
            ? Visibility.Collapsed
            : Visibility.Visible;
    }
}
