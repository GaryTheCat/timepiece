﻿using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Timepiece_V2.Commands;

namespace Timepiece_V2
{
    [DataContract]
    public class TimeEntry : ReactiveViewModelBase
    {
        public TimeEntry() { }

        public TimeEntry(string job, string ticket, string description, DateTime start, DateTime end)
        {
            Job = job;
            Ticket = ticket;
            Start = start;
            Description = description;
            Start = start;
            End = end;
        }

        public TimeEntry(
            Guid id,
            long startTicks, 
            long endTicks, 
            string job, 
            string ticket, 
            string description, 
            string group, 
            string colour)
        {
            Id = id == default ? Guid.NewGuid() : id;
            StartTicks = startTicks;
            EndTicks = endTicks;
            Job = job;
            Ticket = ticket;
            Description = description;
            Group = group;
            Start = new DateTime(startTicks);
            End = new DateTime(endTicks);
            Colour = colour;
        }

        public ICommand ClickCommand { get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public DateTime End { get; set; }

        [DataMember]
        public string Job { get; set; }

        [DataMember]
        public string Ticket { get; set; }

        [DataMember]
        public string Description { get; set; } = string.Empty;

        [DataMember]
        public string Group { get; set; }

        [DataMember]
        public string Colour { get; set; } = "#3D8EF5";

        [DataMember]
        public Guid Id { get; set; }

        public int BorderThickness => IsSelected ? 1 : 0;

        public SolidColorBrush BorderBrush => IsSelected 
            ? new SolidColorBrush(Color.FromRgb(0x3D, 0x8E, 0xF5)) 
            : new SolidColorBrush(Color.FromRgb(0xFF, 0xFF, 0xFF));

        // Used to show a job moniker if this entry has a job.
        public bool ShowJob => !string.IsNullOrEmpty(Job);

        // Used to show a job moniker if this entry has a ticket.
        public bool ShowTicket => !string.IsNullOrEmpty(Ticket);

        public Visibility ShowJobFill => ShowJob ? Visibility.Visible : Visibility.Collapsed;

        public Visibility ShowTicketFill => ShowTicket ? Visibility.Visible: Visibility.Collapsed;

        public long Duration => (this.End - this.Start).Ticks;

        // Duration is displayed as 00:00:00
        public string DisplayDuration => GetDisplayedDuration();

        private string GetDisplayedDuration()
        {
            var duration = (this.End - this.Start);
            return $"{duration.Hours.ToString("00")}h {duration.Minutes.ToString("00")}m";
        }

        private bool isSelected;

        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                this.OnPropertyChanged("IsSelected");
                this.OnPropertyChanged("Background");
            }
        }

        public long StartTicks
        {
            get => this.Start.Ticks;
            set => this.Start = new DateTime(value);
        }

        public long EndTicks
        { 
            get => this.End.Ticks;
            set => this.End = new DateTime(value);
        }

        public TimeEntry Clone()
        {
            return new TimeEntry(Id, StartTicks, EndTicks, Job, Ticket, Description, Group, Colour);
        }
    }
}