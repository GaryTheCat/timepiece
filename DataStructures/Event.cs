﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Timepiece_V2.DataStructures
{
    /// <summary>
    /// Event contains all information for a single instance of a reoccuring event 
    /// being created. 
    /// The DataContract and DataMember tags are used for the JSON serialisation 
    /// and deserialisation. All members tagged with [DataMember] will be serialised.
    /// </summary>
    [DataContract]
    public class Event : ReactiveViewModelBase
    {
        [DataMember]
        public Guid Id { get; set; }

        public string DisplayName => GetDisplayName();

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Job { get; set; }

        [DataMember]
        public string Ticket { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<DayOfWeek> Days { get; set; } = new List<DayOfWeek>();

        [DataMember]
        public Reoccurance Reoccurance { get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public double Duration { get; set; }

        [DataMember]
        public DateTime NextOccurance { get; set; }

        // We don't care if this record is checked when we close.
        public bool IsChecked { get; set; }

        // Displayed name formt is: Scrum Weekly 10:00-10:15 [m, w, f]
        private string GetDisplayName()
        {
            var dayString = "[";
            foreach(var day in Days)
            {
                dayString += day.ToString().ToLower()[0] + ", ";
            }

            dayString = dayString.Substring(0, dayString.Length - 2) + "]";

            var end = Start.AddMinutes(Duration);

            return $"{Name} {Reoccurance} {dayString} {Start.Hour.ToString("00")}:{Start.Minute.ToString("00")}-{end.Hour.ToString("00")}:{end.Minute.ToString("00")}";
        }
    }
}
