﻿using System;

namespace Timepiece_V2.DataStructures
{
    /// <summary>
    /// Day - A reactive class representing a selectable day of the week.
    /// </summary>
    public class Day : ReactiveViewModelBase
    {
        public bool IsEnabled { get; set; }

        public DayOfWeek DayOfWeek { get; set; }
    }
}
