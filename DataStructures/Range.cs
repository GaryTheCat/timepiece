﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timepiece_V2.DataStructures
{
    public class Range<T> where T : IComparable<T>
    {
        public T Lower { get; set; }

        public T Upper { get; set; }

        public Range(T t1, T t2)
        {
            Lower = t1;
            Upper = t2;
        }

        public bool IsInRange(T t)
        {
            return t.CompareTo(Lower) > 0 && t.CompareTo(Upper) < 0;
        }
    }
}
