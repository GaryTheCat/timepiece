﻿using System.Windows;
using System.Windows.Controls;

namespace Timepiece_V2.DataStructures
{
    public interface ITab
    {
        string Id { get; }

        bool IsSelected { get; set; }

        string GetHeaderText();
        
        UserControl GetContent();

        FrameworkElement Accent { get; set; }

        FrameworkElement Header { get; set; }
    }
}
