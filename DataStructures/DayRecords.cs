﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Timepiece_V2.DataStructures
{
    /// <summary>
    /// Day Records - Encapsulates the all information and records about 1 days
    /// worth of work.
    /// </summary>
    public class DayRecords : ReactiveViewModelBase
    {
        // The day is uniquely identified by its date
        public DateTime Id { get; set; }

        // Name of the day of the week
        public string DayName { get; set; }

        // Display name is either Today, Yesterday, or [DayOfWeek]
        public string DisplayName => GetDisplayName();

        public Visibility Collapsed { get; set; } = Visibility.Visible;

        private string GetDisplayName()
        {
            var today = DateTime.Now;
            var day = today.Day;
            var month = today.Month;
            var year = today.Year;

            if (Id.Day == day && Id.Month == month && Id.Year == year)
            {
                return "Today";
            }

            if (Id.Day == day - 1 && Id.Month == month && Id.Year == year)
            {
                return "Yesterday";
            }

            // Use the inbuilt DateTime formatting.
            return Id.ToString("dd MMMM yyyy");
        }

        // All Entries for this day. This list should contain an instance of 
        // every entry, and not aggregate the information in any way. 
        // Aggregated views can be created from this list.
        public ObservableCollection<TimeEntry> TimeEntries { get; set; }
            = new ObservableCollection<TimeEntry>();

        // Displayed time format is 00h 00m
        public string TotalTime => GetTotalTimeString();

        public string GetTotalTimeString()
        {
            var currentTime = new TimeSpan(TimeEntries.Sum(e => e.Duration));

            var hours = ((int)currentTime.TotalHours).ToString("00");
            var minutes = currentTime.Minutes.ToString("00");
            var seconds = currentTime.Seconds.ToString("00");

            double totalMillis = 8 * 60 * 60 * 1000;
            var percentage = (currentTime.TotalMilliseconds / totalMillis) * 100;

            return $"{hours}h {minutes}m";
        }

        public void ToggleCollapsed()
        {
            Collapsed = Collapsed == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }
    }
}
