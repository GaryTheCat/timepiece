﻿using System.Collections.Generic;

namespace Timepiece_V2.Services
{
    public static class WindowService
    {
        public static T GetWindow<T>()
        {
            var type = typeof(T).Name;
            if (windows.ContainsKey(type))
            {
                return (T)windows[type];
            }
            else
            {
                return default;
            }
        }

        public static void RegisterWindow(object window)
        {
            windows.Add(window.GetType().Name, window);
        }

        private static Dictionary<string, object> windows = new Dictionary<string, object>();
    }
}
