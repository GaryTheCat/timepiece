﻿using System;
using System.Collections.Generic;

namespace Timepiece_V2.Services
{
    public static class MessageBus
    {
        public static void Listen<T>(Action a)
        {
            var type = typeof(T).Name;
            if (!callbacks.ContainsKey(type))
            {
                callbacks.Add(type, new List<Action>());
            }

            callbacks[type].Add(a);
        }

        public static void Send<T>()
        {
            var type = typeof(T).Name;
            if (callbacks.ContainsKey(type))
            {
                callbacks[type].ForEach(callback => callback());
            }
        }

        private static Dictionary<string, List<Action>> callbacks = new Dictionary<string, List<Action>>();
    }
}
