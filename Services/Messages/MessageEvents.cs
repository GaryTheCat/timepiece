﻿namespace Timepiece_V2.Services.Messages
{
    public class EntryUpdatedEvent { }

    public class RunningEntryRemovedEvent { }

    public class EntryTimeUpdatedEvent { }

    public class EntrySelectionChangedEvent { }

    public class LeftPaneGotFocusEvent { }

    public class RightPaneGotFocusEvent { }
}
