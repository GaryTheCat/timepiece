﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Timepiece_V2.Notifications
{
    public class ToastNotificationViewModel : ReactiveViewModelBase
    {
        private Timer displayTimer;
        private Storyboard fadeIn;
        private Storyboard fadeOut;
        private UserControl toastControl;
        
        public ToastNotificationViewModel()
        {

        }

        public void Loaded(object o)
        {
            this.toastControl = o as UserControl;

            fadeIn = (Storyboard)this.toastControl.FindResource("ToastFadeIn");
            fadeOut = (Storyboard)this.toastControl.FindResource("ToastFadeOut");

            fadeOut.Completed += FadeOutCompleted;
        }

        public Visibility ToastVisible { get; set; } = Visibility.Collapsed;

        public Visibility ShowSuccessful { get; set; }

        public Visibility ShowUnsuccessful { get; set; }

        public double WindowOpacity { get; set; }

        public void DismissToast()
        {
            ClearDisplayTimer();
            this.WindowOpacity = 0;
            this.ToastVisible = Visibility.Collapsed;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Icon { get; set; }
        
        public void ShowIconToast(string title, string description, int durationSeconds, bool successful)
        {
            if (toastControl == null)
            {
                return;
            }

            ClearDisplayTimer();

            ShowSuccessful = successful ? Visibility.Visible : Visibility.Collapsed;
            ShowUnsuccessful = !successful ? Visibility.Visible : Visibility.Collapsed;

            this.Title = title;
            this.Description = description;

            var millisecondDuration = durationSeconds * 1000;

            ToastVisible = Visibility.Visible;

            toastControl.Dispatcher.Invoke(() =>
            {
                if(toastControl.Opacity == 0)
                {
                    ClearDisplayTimer();
                    fadeIn.Begin(toastControl);
                }
            });

            displayTimer = new Timer(FadeInCompleted, null, millisecondDuration, millisecondDuration);
        }
      
        private void FadeInCompleted(object args)
        {
            toastControl.Dispatcher.Invoke(() =>
            {
                if(toastControl.Opacity == 1)
                {
                    ClearDisplayTimer();
                    fadeOut.Begin(toastControl);
                }
            });
        }

        private void FadeOutCompleted(object sender, EventArgs e)
        {
            ToastVisible = Visibility.Collapsed;
        }

        private void ClearDisplayTimer()
        {
            // Disposing of the timer ensures that if it is restarted before finishing the previous time
            // that the new toast will override the previous, and not get cut short.
            if (displayTimer != null)
            {
                displayTimer.Dispose();
                displayTimer = null;
            }

            fadeIn.Stop(toastControl);
            fadeOut.Stop(toastControl);
        }
    }
}
